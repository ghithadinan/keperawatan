-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2018 at 12:59 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `enurse`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `name`) VALUES
(1, 'Membina hubungan terapeutik dengan pasien/ keluarga'),
(2, 'Menerima pasien baru dan mengorientasikan pasien/ keluarga jika PPJP tidak ada di tempat'),
(3, 'Melakukan tindakan keperawatan pada kliennya berdasarkan rencana perawatan yang telah dibuat PPJP'),
(4, 'Melakukan evaluasi dan dokumentasi tindakan keperawatan'),
(5, 'Mendampingi visite dokter bila PP tidak di tempat'),
(6, 'Menerapkan perilaku caring dalam asuhan keperawatan'),
(7, 'Melaporkan kepada PPJP bila menemukan masalah yang perlu diselesaikan'),
(8, 'Menyiapkan klien untuk pemeriksaan diagnostic, laboratorium, pengobatan, dan tindakan'),
(9, 'Memberikan pendidikan pada pasien/keluarga'),
(10, 'Memelihara sarana, fasilitas keperawatan, dan kebersihan ruangan'),
(11, 'Mengikuti kegiatan ilmiah (RDK/refleksi diskusi kasus, ronde keperawatan, siang klinik, dll)'),
(12, 'Melakukan kompetensi dasar keperawatan'),
(13, 'Melakukan kompetensi inti PK I Keperawatan'),
(14, 'Melakukan komunikasi interpersonal dalam melaksanakan tindakan keperawatan'),
(15, 'Menerapkan prinsp-prinsip infeksi nosokomial'),
(16, 'Menerapkan prinsip keselamatan pasien'),
(17, 'Menerapkan proses keperawatan kebutuhan dasar'),
(18, 'Melakukan perawatan luka sederhana'),
(19, 'Melakukan ambulasi pasien'),
(20, 'Memfasilitasi pemenuhan kebutuhan oksigen dasar'),
(21, 'Memfasilitasi pemenuhan kebutuhan cairan dan elektrolit dasar'),
(22, 'Memfasilitasi pemberian darah dan produk darah'),
(23, 'Memfasilitasi pemberian obat sederhana'),
(24, 'Melakukan pendokumentasian tindakan keperawatan'),
(25, 'Melakukan peningkatan mutu asuhan keperawatan'),
(26, 'Melakukan kompetensi inti PK II Keperawatan');

-- --------------------------------------------------------

--
-- Table structure for table `golongan`
--

CREATE TABLE `golongan` (
  `id_gol` int(2) NOT NULL,
  `nama_gol` varchar(5) NOT NULL,
  `pangkat` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `golongan`
--

INSERT INTO `golongan` (`id_gol`, `nama_gol`, `pangkat`) VALUES
(1, 'II/c', 'Pengatur'),
(3, 'II/d', 'Pengatur TK I'),
(4, 'III/a', 'Penata Muda'),
(5, 'III/b', 'Penata Muda Tingkat I'),
(6, 'III/c', 'Penata'),
(7, 'III/d', 'Penata Tingkat I'),
(8, 'IV/a', 'Pembina'),
(9, 'IV/b', 'Pembina Tingkat I'),
(10, 'IV/c', 'Pembina Utama Muda'),
(11, 'IV/d', 'Pembina Utama Madya'),
(12, 'IV/e', 'Pembina Utama');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `nama_jabatan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `nama_jabatan`) VALUES
(1, 'Kepala Bidang'),
(3, 'Kepala Seksi'),
(4, 'Staff Keperawatan'),
(5, 'Kepala Ruangan'),
(6, 'Wakaru/Ccm'),
(7, 'Pelaksana');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `id_shift` int(11) NOT NULL,
  `tgl_jadwal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `id_nurse`, `id_ruangan`, `id_shift`, `tgl_jadwal`) VALUES
(1, 11, 4, 1, '2018-05-28'),
(2, 11, 4, 2, '2018-05-01'),
(14, 10, 4, 1, '2018-05-01'),
(15, 10, 4, 1, '2018-05-02'),
(16, 10, 4, 1, '2018-05-03'),
(17, 10, 4, 1, '2018-05-04'),
(18, 10, 4, 2, '2018-05-05'),
(19, 10, 4, 5, '2018-01-01');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_kelamin`
--

CREATE TABLE `jenis_kelamin` (
  `jk` int(11) NOT NULL,
  `nama_jk` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_kelamin`
--

INSERT INTO `jenis_kelamin` (`jk`, `nama_jk`) VALUES
(1, 'L'),
(2, 'P');

-- --------------------------------------------------------

--
-- Table structure for table `laporan_bulanan`
--

CREATE TABLE `laporan_bulanan` (
  `id` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `bulan` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laporan_bulanan`
--

INSERT INTO `laporan_bulanan` (`id`, `id_nurse`, `judul`, `isi`, `bulan`) VALUES
(2, 10, 'bb', '<p>bbbbbbbbb</p>\r\n', '2018-04'),
(3, 10, 'Maret', 'Ini Laporan Bulan Maret', '2018-03');

-- --------------------------------------------------------

--
-- Table structure for table `laporan_harian`
--

CREATE TABLE `laporan_harian` (
  `id` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laporan_harian`
--

INSERT INTO `laporan_harian` (`id`, `id_nurse`, `judul`, `isi`, `tanggal`) VALUES
(8, 8, 'aaa', '<p>aaaaa</p>\r\n', '2018-04-09');

-- --------------------------------------------------------

--
-- Table structure for table `laporan_tahunan`
--

CREATE TABLE `laporan_tahunan` (
  `id` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `tahun` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laporan_tahunan`
--

INSERT INTO `laporan_tahunan` (`id`, `id_nurse`, `judul`, `isi`, `tahun`) VALUES
(1, 10, 'tahunan', '<p>tahun berjalans</p>\r\n', 2018),
(2, 9, 'Abdi teh Dini', '<p>Uhun teh dini</p>\r\n', 2018);

-- --------------------------------------------------------

--
-- Table structure for table `logbooks`
--

CREATE TABLE `logbooks` (
  `id` int(11) NOT NULL,
  `nurse_id` int(11) DEFAULT NULL,
  `pk_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `shift_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `patient_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logbooks`
--

INSERT INTO `logbooks` (`id`, `nurse_id`, `pk_id`, `activity_id`, `shift_id`, `date`, `time`, `patient_name`) VALUES
(40, 10, 4, 1, 2, '2018-05-01', '14:00:00', 'Tn. Amran'),
(41, 10, 4, 26, 2, '2018-05-01', '15:00:00', 'Tn Mirdad'),
(42, 10, 4, 0, 2, '2018-05-01', '16:00:00', ''),
(43, 10, 4, 0, 2, '2018-05-01', '17:00:00', ''),
(44, 10, 4, 0, 2, '2018-05-01', '18:00:00', ''),
(45, 10, 4, 0, 2, '2018-05-01', '19:00:00', ''),
(46, 10, 4, 0, 2, '2018-05-01', '20:00:00', ''),
(47, 10, 4, 2, 1, '2018-05-01', '07:00:00', 'Tn. Amran'),
(48, 10, 4, 25, 1, '2018-05-01', '08:00:00', 'Tn Mirdad dan Tn. Jaja'),
(49, 10, 4, 3, 1, '2018-05-01', '09:00:00', ''),
(50, 10, 4, 1, 1, '2018-05-01', '10:00:00', ''),
(51, 10, 4, 1, 1, '2018-05-01', '11:00:00', ''),
(52, 10, 4, 0, 1, '2018-05-01', '12:00:00', ''),
(53, 10, 4, 0, 1, '2018-05-01', '13:00:00', ''),
(54, 10, 4, 0, 1, '2018-05-01', '14:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `nurses`
--

CREATE TABLE `nurses` (
  `id_nurse` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nip` varchar(18) NOT NULL,
  `id_gol` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jk` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `id_status_kepegawaian` int(11) NOT NULL,
  `id_pk` int(11) NOT NULL,
  `id_pendidikan` int(11) NOT NULL,
  `id_jurusan` varchar(100) NOT NULL,
  `id_pelatihan` text NOT NULL,
  `gelar_depan` varchar(5) NOT NULL,
  `gelar_belakang` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL DEFAULT '0d7cb-hepdi.jpg',
  `password` varchar(50) NOT NULL DEFAULT 'password'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurses`
--

INSERT INTO `nurses` (`id_nurse`, `nama`, `nip`, `id_gol`, `id_jabatan`, `tanggal_lahir`, `jk`, `id_ruangan`, `id_status_kepegawaian`, `id_pk`, `id_pendidikan`, `id_jurusan`, `id_pelatihan`, `gelar_depan`, `gelar_belakang`, `image`, `password`) VALUES
(7, 'Osep Hernandi', '197101021996031005', 8, 1, '1971-01-02', 1, 9, 1, 1, 5, 'Master Keperawatan', 'BCTLS', 'H.', 'S.Sos., S.Kep., M.Kep', '7aae3-1.jpeg', 'hepdi'),
(8, 'Firnia', '199203212018122315', 1, 7, '1992-03-21', 2, 4, 1, 1, 2, 'Keperawatan', 'BTCLS', '', '', '', 'password'),
(9, 'Dini Andini', '197910082002122002', 4, 5, '1979-10-10', 2, 8, 1, 3, 2, 'Keperawatan', 'SMCC', '', '', '', 'password'),
(10, 'Aar Arisah', '196912071995032001', 8, 5, '1969-12-07', 2, 10, 1, 4, 4, 'Keperawatan', 'PMP', '', 'S.Kep, Ners', 'c13c4-aar-arisah.jpg', 'password'),
(11, 'Rifa Arsika Dewi', '197711132008012007', 6, 5, '1977-11-11', 2, 4, 1, 3, 4, 'Keperawatan', 'CCD', 'Hj.', 'S.Kep, Ners', 'a461b-rifa-arsika-dewi.jpg', 'password'),
(12, 'Ai Mulyati', '197903082008012004', 4, 7, '1979-03-08', 1, 4, 1, 3, 2, 'Keperawatan', 'SSC', '', '', '', 'password');

--
-- Triggers `nurses`
--
DELIMITER $$
CREATE TRIGGER `delete_simk` AFTER DELETE ON `nurses` FOR EACH ROW DELETE FROM simk WHERE simk.id_nurse = old.id_nurse
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `tambah_simk` AFTER INSERT ON `nurses` FOR EACH ROW INSERT INTO simk(id_simk,id_nurse,nira,tmt,status)
VALUES ('', NEW.id_nurse, '', '','')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE `pendidikan` (
  `id_pendidikan` int(11) NOT NULL,
  `nama_pendidikan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan`
--

INSERT INTO `pendidikan` (`id_pendidikan`, `nama_pendidikan`) VALUES
(1, 'SPK'),
(2, 'DIII'),
(3, 'S1'),
(4, 'S1 Keperawatan + Ners'),
(5, 'S2');

-- --------------------------------------------------------

--
-- Table structure for table `pk_activity`
--

CREATE TABLE `pk_activity` (
  `activity_id` int(11) NOT NULL,
  `pk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pk_activity`
--

INSERT INTO `pk_activity` (`activity_id`, `pk_id`) VALUES
(1, 0),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rencana_kegiatan_bulanan`
--

CREATE TABLE `rencana_kegiatan_bulanan` (
  `id_rencana_bulanan` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `jenis_kegiatan` varchar(100) NOT NULL,
  `rencana_anggaran` int(100) NOT NULL,
  `bulan` date NOT NULL,
  `rincian` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rencana_kegiatan_bulanan`
--

INSERT INTO `rencana_kegiatan_bulanan` (`id_rencana_bulanan`, `id_nurse`, `id_ruangan`, `jenis_kegiatan`, `rencana_anggaran`, `bulan`, `rincian`) VALUES
(9, 10, 4, 'Pensiun', 15000000, '2018-01-01', '<p>ssss</p>\r\n'),
(10, 10, 4, 'Cuci Tangan Memakai Tanah Liat', 100000000, '2018-06-01', '<p>1. Mencuci tangan memakai tanah liat yang sudah dikeringkan</p>\r\n\r\n<p>2. Kemudian dikeringkan di bawah terik panas matahari</p>\r\n\r\n<p>3. Wew</p>\r\n'),
(11, 9, 8, 'Pembersian Alat Tangkap', 500000, '2018-07-01', '<p>Membersihkan peralatang untuk menangkap ikan</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `rencana_kegiatan_tahunan`
--

CREATE TABLE `rencana_kegiatan_tahunan` (
  `id_rencana_tahunan` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `jenis_kegiatan` varchar(100) NOT NULL,
  `rencana_anggaran` int(100) NOT NULL,
  `tahun` year(4) NOT NULL,
  `rincian` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rencana_kegiatan_tahunan`
--

INSERT INTO `rencana_kegiatan_tahunan` (`id_rencana_tahunan`, `id_nurse`, `id_ruangan`, `jenis_kegiatan`, `rencana_anggaran`, `tahun`, `rincian`) VALUES
(1, 10, 4, 'Dekorasi Pantry', 2000000, 2019, '<p>Rencana Kegiatan Mendekorasi Pantry Ruangan</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `ruangan`
--

CREATE TABLE `ruangan` (
  `id_ruangan` int(11) NOT NULL,
  `nama_ruangan` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruangan`
--

INSERT INTO `ruangan` (`id_ruangan`, `nama_ruangan`) VALUES
(1, 'Melati'),
(2, 'Mawar'),
(3, 'ICU'),
(4, 'IGD'),
(5, 'Dahlia'),
(6, 'Kenanga'),
(7, 'VIP'),
(8, 'Thalasemia'),
(9, 'Bidang Keperawatan'),
(10, 'Bougenvile Lt. I');

-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE `shift` (
  `id_shift` int(11) NOT NULL,
  `nama_shift` varchar(30) NOT NULL,
  `jam_masuk` time DEFAULT NULL,
  `jam_pulang` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shift`
--

INSERT INTO `shift` (`id_shift`, `nama_shift`, `jam_masuk`, `jam_pulang`) VALUES
(1, 'P', '07:00:00', '14:00:00'),
(2, 'S', '14:00:00', '20:00:00'),
(3, 'M', '20:00:00', '08:00:00'),
(4, 'L/M', NULL, NULL),
(5, 'L', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `simk`
--

CREATE TABLE `simk` (
  `id_simk` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `nira` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `simk`
--

INSERT INTO `simk` (`id_simk`, `id_nurse`, `nira`) VALUES
(4, 7, '8766500'),
(5, 8, '15677'),
(6, 9, '32070271437'),
(7, 10, '32070026581'),
(8, 11, '32070030214'),
(9, 12, '32070026224');

-- --------------------------------------------------------

--
-- Table structure for table `status_kepegawaian`
--

CREATE TABLE `status_kepegawaian` (
  `id_status_kepegawaian` int(11) NOT NULL,
  `status_kepegawaian` varchar(50) NOT NULL,
  `keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_kepegawaian`
--

INSERT INTO `status_kepegawaian` (`id_status_kepegawaian`, `status_kepegawaian`, `keterangan`) VALUES
(1, 'PNS', 'Pegwai Negeri Sipil'),
(2, 'TKK', 'Tenaga Kerja Kontrak'),
(3, 'Magang', 'Magang');

-- --------------------------------------------------------

--
-- Table structure for table `tugas_pokok`
--

CREATE TABLE `tugas_pokok` (
  `id_pk` int(11) NOT NULL,
  `nama_pk` varchar(5) NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tugas_pokok`
--

INSERT INTO `tugas_pokok` (`id_pk`, `nama_pk`, `keterangan`) VALUES
(1, 'PK 1', ''),
(2, 'PK 2', ''),
(3, 'PK 3', ''),
(4, 'PK 4', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `id_nurse`, `username`, `password`) VALUES
(1, 0, 'admin', 'admin'),
(2, 7, '8766500', '8766500');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `golongan`
--
ALTER TABLE `golongan`
  ADD PRIMARY KEY (`id_gol`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indexes for table `jenis_kelamin`
--
ALTER TABLE `jenis_kelamin`
  ADD PRIMARY KEY (`jk`);

--
-- Indexes for table `laporan_bulanan`
--
ALTER TABLE `laporan_bulanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan_harian`
--
ALTER TABLE `laporan_harian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan_tahunan`
--
ALTER TABLE `laporan_tahunan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logbooks`
--
ALTER TABLE `logbooks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurses`
--
ALTER TABLE `nurses`
  ADD PRIMARY KEY (`id_nurse`);

--
-- Indexes for table `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD PRIMARY KEY (`id_pendidikan`);

--
-- Indexes for table `pk_activity`
--
ALTER TABLE `pk_activity`
  ADD PRIMARY KEY (`activity_id`,`pk_id`);

--
-- Indexes for table `rencana_kegiatan_bulanan`
--
ALTER TABLE `rencana_kegiatan_bulanan`
  ADD PRIMARY KEY (`id_rencana_bulanan`);

--
-- Indexes for table `rencana_kegiatan_tahunan`
--
ALTER TABLE `rencana_kegiatan_tahunan`
  ADD PRIMARY KEY (`id_rencana_tahunan`);

--
-- Indexes for table `ruangan`
--
ALTER TABLE `ruangan`
  ADD PRIMARY KEY (`id_ruangan`);

--
-- Indexes for table `shift`
--
ALTER TABLE `shift`
  ADD PRIMARY KEY (`id_shift`);

--
-- Indexes for table `simk`
--
ALTER TABLE `simk`
  ADD PRIMARY KEY (`id_simk`);

--
-- Indexes for table `status_kepegawaian`
--
ALTER TABLE `status_kepegawaian`
  ADD PRIMARY KEY (`id_status_kepegawaian`);

--
-- Indexes for table `tugas_pokok`
--
ALTER TABLE `tugas_pokok`
  ADD PRIMARY KEY (`id_pk`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `golongan`
--
ALTER TABLE `golongan`
  MODIFY `id_gol` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `jenis_kelamin`
--
ALTER TABLE `jenis_kelamin`
  MODIFY `jk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `laporan_bulanan`
--
ALTER TABLE `laporan_bulanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `laporan_harian`
--
ALTER TABLE `laporan_harian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `laporan_tahunan`
--
ALTER TABLE `laporan_tahunan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `logbooks`
--
ALTER TABLE `logbooks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `nurses`
--
ALTER TABLE `nurses`
  MODIFY `id_nurse` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pendidikan`
--
ALTER TABLE `pendidikan`
  MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `rencana_kegiatan_bulanan`
--
ALTER TABLE `rencana_kegiatan_bulanan`
  MODIFY `id_rencana_bulanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `rencana_kegiatan_tahunan`
--
ALTER TABLE `rencana_kegiatan_tahunan`
  MODIFY `id_rencana_tahunan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ruangan`
--
ALTER TABLE `ruangan`
  MODIFY `id_ruangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `shift`
--
ALTER TABLE `shift`
  MODIFY `id_shift` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `simk`
--
ALTER TABLE `simk`
  MODIFY `id_simk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `status_kepegawaian`
--
ALTER TABLE `status_kepegawaian`
  MODIFY `id_status_kepegawaian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tugas_pokok`
--
ALTER TABLE `tugas_pokok`
  MODIFY `id_pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
