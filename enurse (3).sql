-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2018 at 12:34 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `enurse`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `name`) VALUES
(1, 'Membina hubungan terapeutik dengan pasien/ keluarga'),
(2, 'Menerima pasien baru dan mengorientasikan pasien/ keluarga jika PPJP tidak ada di tempat'),
(3, 'Melakukan tindakan keperawatan pada kliennya berdasarkan rencana perawatan yang telah dibuat PPJP'),
(4, 'Melakukan evaluasi dan dokumentasi tindakan keperawatan'),
(5, 'Mendampingi visite dokter bila PP tidak di tempat'),
(6, 'Menerapkan perilaku caring dalam asuhan keperawatan'),
(7, 'Melaporkan kepada PPJP bila menemukan masalah yang perlu diselesaikan'),
(8, 'Menyiapkan klien untuk pemeriksaan diagnostic, laboratorium, pengobatan, dan tindakan'),
(9, 'Memberikan pendidikan pada pasien/keluarga'),
(10, 'Memelihara sarana, fasilitas keperawatan, dan kebersihan ruangan'),
(11, 'Mengikuti kegiatan ilmiah (RDK/refleksi diskusi kasus, ronde keperawatan, siang klinik, dll)'),
(12, 'Melakukan kompetensi dasar keperawatan'),
(13, 'Melakukan kompetensi inti PK I Keperawatan'),
(14, 'Melakukan komunikasi interpersonal dalam melaksanakan tindakan keperawatan'),
(15, 'Menerapkan prinsp-prinsip infeksi nosokomial'),
(16, 'Menerapkan prinsip keselamatan pasien'),
(17, 'Menerapkan proses keperawatan kebutuhan dasar'),
(18, 'Melakukan perawatan luka sederhana'),
(19, 'Melakukan ambulasi pasien'),
(20, 'Memfasilitasi pemenuhan kebutuhan oksigen dasar'),
(21, 'Memfasilitasi pemenuhan kebutuhan cairan dan elektrolit dasar'),
(22, 'Memfasilitasi pemberian darah dan produk darah'),
(23, 'Memfasilitasi pemberian obat sederhana'),
(24, 'Melakukan pendokumentasian tindakan keperawatan'),
(25, 'Melakukan peningkatan mutu asuhan keperawatan'),
(26, 'Melakukan kompetensi inti PK II Keperawatan'),
(27, 'Memfasilitasi pemenuhan kebutuhan pada gangguan eliminasi urin'),
(28, 'Memfasilitasi pemenuhan kebutuhan pada gangguan eliminasi fekal'),
(29, 'Memfasilitasi pemenuhan kebutuhan cairan dan elektrolit lanjutan'),
(30, 'Memfasilitasi pemenuhan kebutuhan oksigen lanjutan'),
(31, 'Memfasilitasi pemberian obat lanjutan'),
(32, 'Memfasilitasi pemenuhan kebutuhan pada gangguan istirahat dan tidur'),
(33, 'Melakukan tindakan disaster saat bencana'),
(34, 'Mempersiapkan kepulangan pasien'),
(35, 'Mempersiapkan pasien untuk prosedur diagnostik'),
(36, 'Memfasilitasi pemenuhan kebutuhan nutrisi melalui per enteral'),
(37, 'Memfasilitasi pemenuhan kebutuhan nutrisi melalui parenteral'),
(38, 'Memfasilitasi pasien melakukan latihan fisik'),
(39, 'Melakukan tindakan keperawatan pada pasien menjelang ajal'),
(40, 'Melakukan dukungan bagi pasien dan keluarga'),
(41, 'Melakukan kompetensi inti Keperawatan PK III  '),
(42, 'Melaksanakan pengkajian keperawatan dan kesehatan secara sistematis dan komprehensif'),
(43, 'Menyususn dan mendokumentasikan rencana keperawatan'),
(44, 'Mengelola intervensi keperawatan'),
(45, 'Menyusun rencana dan melaksanakan pembelajaran kepada'),
(46, 'Merencanakan discharge planning'),
(47, 'Melakukan tindakan keperawatan pada pasien yang kehilangan/ berduka'),
(48, 'Melakukan manajemen nyeri'),
(49, 'Melakukan perawatan luka lanjutan'),
(50, 'Melakukan verifikasi hasil implementasi tindakan keperawatan'),
(51, 'Mengelola rencana asuhan pasien yang menjalani preoperatif'),
(52, 'Mengelola rencana asuhan pasien post operatif'),
(53, 'Mengevaluasi keefektifan pelaksanaan rencana asuhan'),
(54, 'Melakukan pengembangan profesional dan peserta didik'),
(55, 'Melakukan kontrak dengan pasien/ keluarga yang menjadi tanggung jawabnya dari pasien masuk sampai pasien pulang pada awal pasien'),
(56, 'Melakukan pengkajian terhadap klien baru atau melengkapi pengkajian yang sudah dilakukan PP/PA pada sore, malam atau hari libur'),
(57, 'Menetapkan rencana asuhan keperawatan berdasarkan analisi standar rencana perawatan sesuai dengan hasil pengkajian'),
(58, 'Menjelaskan rencana perawatan yang sudah ditetapkan pada PA dibawah tanggung jawabnya sesuai klien yang dirawat (preconference)'),
(59, 'Menetapkan PA yang bertanggung jawab pada setiap klien'),
(60, 'Melakukan bimbingan dan evaluasi kepada PA dalam kepatuhan terhadap SPO keperawatan'),
(61, 'Memonitor dokumentasi yang dilakukan oleh PA'),
(62, 'Membantu dan memfasilitasi terlaksananya kegiatan PA'),
(63, 'Melakukan kolaborasi dengan tim kesehatan lain sesuai kebutuhan pasien  (permasalahan komplek)'),
(64, 'Melakukan kegiatan serah terima klien dibawah tanggung jawabnya bersama dengan PA.'),
(65, 'Mendampingi dokter visite klien dibawah tanggung jawabnya. Bila PP tidak ada, visite didampingi oleh PA sesuai team nya'),
(66, 'Melakukan evaluasi asuhan keperawatan dan membuat catatan perkembangan klien setiap hari'),
(67, 'Melakukan pertemuan/ memfasilitasi pertemuan tim kesehatan dengan klien / keluarga untuk membahas kondisi keperawatan klien (bergantung pasa kondisi klien)'),
(68, 'Memberikan pendidikan kesehatan kepada pasien/ keluarga'),
(69, 'Membuat perencanaan pulang'),
(70, 'Melakuikan kerjasama dengan Case Manager dalam mengidentifikasi isu yang memerlukan pembuktian sehingga tercipta evidence based practice (EBP)');

-- --------------------------------------------------------

--
-- Table structure for table `golongan`
--

CREATE TABLE `golongan` (
  `id_gol` int(2) NOT NULL,
  `nama_gol` varchar(5) NOT NULL,
  `pangkat` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `golongan`
--

INSERT INTO `golongan` (`id_gol`, `nama_gol`, `pangkat`) VALUES
(1, 'II/c', 'Pengatur'),
(3, 'II/d', 'Pengatur TK I'),
(4, 'III/a', 'Penata Muda'),
(5, 'III/b', 'Penata Muda Tingkat I'),
(6, 'III/c', 'Penata'),
(7, 'III/d', 'Penata Tingkat I'),
(8, 'IV/a', 'Pembina'),
(9, 'IV/b', 'Pembina Tingkat I'),
(10, 'IV/c', 'Pembina Utama Muda'),
(11, 'IV/d', 'Pembina Utama Madya'),
(12, 'IV/e', 'Pembina Utama');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `nama_jabatan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `nama_jabatan`) VALUES
(1, 'Kepala Bidang'),
(3, 'Kepala Seksi'),
(4, 'Staff Keperawatan'),
(5, 'Kepala Ruangan'),
(6, 'Wakaru/Ccm'),
(7, 'Pelaksana');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `id_shift` int(11) NOT NULL,
  `tgl_jadwal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `id_nurse`, `id_ruangan`, `id_shift`, `tgl_jadwal`) VALUES
(1, 11, 4, 1, '2018-05-28'),
(2, 11, 4, 5, '2018-05-01'),
(20, 10, 10, 5, '2018-05-01'),
(21, 10, 10, 1, '2018-05-02'),
(22, 10, 10, 1, '2018-05-03'),
(23, 10, 10, 1, '2018-05-04'),
(24, 10, 10, 1, '2018-05-05'),
(25, 10, 10, 5, '2018-05-06'),
(26, 10, 10, 1, '2018-05-07'),
(27, 10, 10, 1, '2018-05-08'),
(28, 10, 10, 1, '2018-05-09'),
(29, 10, 10, 5, '2018-05-10'),
(30, 10, 10, 1, '2018-05-11'),
(31, 10, 10, 1, '2018-05-12'),
(32, 10, 10, 5, '2018-05-13'),
(33, 10, 10, 1, '2018-05-14'),
(34, 10, 10, 1, '2018-05-15'),
(35, 11, 4, 1, '2018-05-02'),
(36, 10, 10, 1, '2018-05-18'),
(37, 10, 10, 1, '2018-05-19'),
(38, 10, 10, 5, '2018-05-20'),
(39, 10, 10, 1, '2018-05-16'),
(40, 10, 10, 1, '2018-05-17'),
(41, 10, 10, 1, '2018-05-21'),
(42, 10, 10, 1, '2018-05-22'),
(43, 10, 10, 1, '2018-05-23'),
(44, 10, 10, 1, '2018-05-24'),
(45, 10, 10, 1, '2018-05-25'),
(46, 10, 10, 1, '2018-05-26'),
(47, 10, 10, 5, '2018-05-27'),
(48, 10, 10, 5, '2018-05-29'),
(49, 10, 10, 1, '2018-05-28'),
(50, 10, 10, 1, '2018-05-30'),
(51, 10, 10, 1, '2018-05-31'),
(52, 11, 4, 1, '2018-01-01'),
(53, 11, 4, 1, '2018-01-02'),
(54, 16, 10, 5, '2018-05-01'),
(55, 16, 10, 1, '2018-05-02'),
(56, 16, 10, 1, '2018-05-03'),
(57, 16, 10, 1, '2018-05-04'),
(58, 16, 10, 1, '2018-05-05'),
(59, 16, 10, 5, '2018-05-06'),
(60, 16, 10, 1, '2018-05-07'),
(61, 16, 10, 1, '2018-05-08'),
(62, 16, 10, 1, '2018-05-09'),
(63, 16, 10, 5, '2018-05-10'),
(64, 16, 10, 1, '2018-05-11'),
(65, 16, 10, 1, '2018-05-12'),
(66, 16, 10, 5, '2018-05-13'),
(67, 16, 10, 1, '2018-05-14'),
(68, 16, 10, 1, '2018-05-15'),
(69, 16, 10, 1, '2018-05-16'),
(70, 16, 10, 1, '2018-05-17'),
(71, 16, 10, 1, '2018-05-18'),
(72, 16, 10, 1, '2018-05-19'),
(73, 16, 10, 5, '2018-05-20'),
(74, 16, 10, 1, '2018-05-21'),
(75, 16, 10, 1, '2018-05-22'),
(76, 16, 10, 1, '2018-05-23'),
(77, 16, 10, 1, '2018-05-24'),
(78, 16, 10, 1, '2018-05-25'),
(79, 16, 10, 1, '2018-05-26'),
(80, 16, 10, 5, '2018-05-27'),
(81, 16, 10, 1, '2018-05-28'),
(82, 16, 10, 5, '2018-05-29'),
(83, 16, 10, 1, '2018-05-30'),
(84, 16, 10, 1, '2018-05-31'),
(85, 37, 10, 5, '2018-05-01'),
(86, 37, 10, 1, '2018-05-02'),
(87, 37, 10, 1, '2018-05-03'),
(88, 37, 10, 3, '2018-05-04'),
(89, 37, 10, 4, '2018-05-05'),
(90, 37, 10, 1, '2018-05-06'),
(91, 37, 10, 1, '2018-05-07'),
(92, 37, 10, 3, '2018-05-08'),
(93, 37, 10, 4, '2018-05-09'),
(94, 37, 10, 5, '2018-05-10'),
(95, 37, 10, 5, '2018-05-11'),
(96, 37, 10, 2, '2018-05-12'),
(97, 37, 10, 2, '2018-05-13'),
(98, 37, 10, 2, '2018-05-14'),
(99, 37, 10, 3, '2018-05-15'),
(100, 37, 10, 4, '2018-05-16'),
(101, 37, 10, 5, '2018-05-17'),
(102, 37, 10, 1, '2018-05-18'),
(103, 37, 10, 1, '2018-05-19'),
(104, 37, 10, 2, '2018-05-20'),
(105, 37, 10, 3, '2018-05-21'),
(106, 37, 10, 4, '2018-05-22'),
(107, 37, 10, 5, '2018-05-23'),
(108, 37, 10, 1, '2018-05-24'),
(109, 37, 10, 5, '2018-05-25'),
(110, 37, 10, 1, '2018-05-26'),
(111, 37, 10, 3, '2018-05-27'),
(112, 37, 10, 4, '2018-05-28'),
(113, 37, 10, 5, '2018-05-29'),
(114, 37, 10, 1, '2018-05-30'),
(115, 37, 10, 1, '2018-05-31'),
(116, 38, 10, 1, '2018-05-01'),
(117, 38, 10, 1, '2018-05-02'),
(118, 38, 10, 1, '2018-05-03'),
(119, 38, 10, 3, '2018-05-04'),
(120, 38, 10, 4, '2018-05-05'),
(121, 38, 10, 5, '2018-05-06'),
(122, 38, 10, 1, '2018-05-07'),
(123, 38, 10, 1, '2018-05-08'),
(124, 38, 10, 2, '2018-05-09'),
(125, 38, 10, 3, '2018-05-10'),
(126, 38, 10, 4, '2018-05-11'),
(127, 38, 10, 5, '2018-05-12'),
(128, 38, 10, 5, '2018-05-13'),
(129, 38, 10, 1, '2018-05-14'),
(130, 38, 10, 1, '2018-05-15'),
(131, 38, 10, 5, '2018-05-16'),
(132, 38, 10, 2, '2018-05-17'),
(133, 38, 10, 3, '2018-05-18'),
(134, 38, 10, 4, '2018-05-19'),
(135, 38, 10, 5, '2018-05-20'),
(136, 38, 10, 5, '2018-05-21'),
(137, 38, 10, 1, '2018-05-22'),
(138, 38, 10, 2, '2018-05-23'),
(139, 38, 10, 3, '2018-05-24'),
(140, 38, 10, 4, '2018-05-25'),
(141, 38, 10, 5, '2018-05-26'),
(142, 38, 10, 1, '2018-05-27'),
(143, 38, 10, 1, '2018-05-28'),
(144, 38, 10, 1, '2018-05-29'),
(145, 38, 10, 3, '2018-05-30'),
(146, 38, 10, 4, '2018-05-31'),
(147, 39, 10, 4, '2018-05-01'),
(148, 39, 10, 5, '2018-05-02'),
(149, 39, 10, 1, '2018-05-03'),
(150, 39, 10, 1, '2018-05-04'),
(151, 39, 10, 2, '2018-05-05'),
(152, 39, 10, 3, '2018-05-06'),
(153, 39, 10, 4, '2018-05-07'),
(154, 39, 10, 5, '2018-05-08'),
(155, 39, 10, 1, '2018-05-09'),
(156, 39, 10, 2, '2018-05-10'),
(157, 39, 10, 3, '2018-05-11'),
(158, 39, 10, 4, '2018-05-12'),
(159, 39, 10, 5, '2018-05-13'),
(160, 39, 10, 1, '2018-05-14'),
(161, 39, 10, 1, '2018-05-15'),
(162, 39, 10, 2, '2018-05-16'),
(163, 39, 10, 3, '2018-05-17'),
(164, 39, 10, 4, '2018-05-18'),
(165, 39, 10, 5, '2018-05-19'),
(166, 39, 10, 1, '2018-05-20'),
(167, 39, 10, 1, '2018-05-21'),
(168, 39, 10, 5, '2018-05-22'),
(169, 39, 10, 2, '2018-05-23'),
(170, 39, 10, 3, '2018-05-24'),
(171, 39, 10, 4, '2018-05-25'),
(172, 39, 10, 5, '2018-05-26'),
(173, 39, 10, 5, '2018-05-27'),
(174, 39, 10, 1, '2018-05-28'),
(175, 39, 10, 2, '2018-05-29'),
(176, 39, 10, 3, '2018-05-30'),
(177, 39, 10, 4, '2018-05-31'),
(178, 11, 4, 1, '2018-05-03'),
(179, 11, 4, 1, '2018-05-04'),
(180, 11, 4, 1, '2018-05-05'),
(181, 11, 4, 5, '2018-05-06'),
(182, 11, 4, 1, '2018-05-07'),
(183, 11, 4, 1, '2018-05-08'),
(184, 11, 4, 1, '2018-05-09'),
(185, 11, 4, 5, '2018-05-10'),
(186, 11, 4, 1, '2018-05-11'),
(187, 11, 4, 1, '2018-05-12'),
(188, 11, 4, 5, '2018-05-13'),
(189, 11, 4, 1, '2018-05-14'),
(190, 11, 4, 1, '2018-05-15'),
(191, 11, 4, 1, '2018-05-16'),
(192, 11, 4, 1, '2018-05-17'),
(193, 11, 4, 1, '2018-05-18'),
(194, 11, 4, 1, '2018-05-19'),
(195, 11, 4, 5, '2018-05-20'),
(196, 11, 4, 1, '2018-05-21'),
(197, 11, 4, 1, '2018-05-22'),
(198, 11, 4, 1, '2018-05-23'),
(199, 11, 4, 1, '2018-05-24'),
(200, 11, 4, 1, '2018-05-25'),
(201, 11, 4, 1, '2018-05-26'),
(202, 11, 4, 5, '2018-05-27'),
(203, 11, 4, 5, '2018-05-29'),
(204, 11, 4, 1, '2018-05-30'),
(205, 11, 4, 1, '2018-05-31'),
(206, 12, 4, 5, '2018-05-01'),
(207, 12, 4, 1, '2018-05-02'),
(208, 12, 4, 1, '2018-05-03'),
(209, 12, 4, 3, '2018-05-04'),
(210, 12, 4, 4, '2018-05-05'),
(211, 12, 4, 1, '2018-05-06'),
(212, 12, 4, 1, '2018-05-07'),
(213, 12, 4, 3, '2018-05-08'),
(214, 12, 4, 4, '2018-05-09'),
(215, 12, 4, 5, '2018-05-10'),
(216, 12, 4, 5, '2018-05-11'),
(217, 12, 4, 2, '2018-05-12'),
(218, 12, 4, 2, '2018-05-13'),
(219, 12, 4, 2, '2018-05-14'),
(220, 12, 4, 3, '2018-05-15'),
(221, 12, 4, 4, '2018-05-16'),
(222, 12, 4, 5, '2018-05-17'),
(223, 12, 4, 1, '2018-05-18'),
(224, 12, 4, 1, '2018-05-19'),
(225, 12, 4, 2, '2018-05-20'),
(226, 12, 4, 3, '2018-05-21'),
(227, 12, 4, 4, '2018-05-22'),
(228, 12, 4, 5, '2018-05-23'),
(229, 12, 4, 1, '2018-05-24'),
(230, 12, 4, 5, '2018-05-25'),
(231, 12, 4, 1, '2018-05-26'),
(232, 12, 4, 3, '2018-05-27'),
(233, 12, 4, 4, '2018-05-28'),
(234, 12, 4, 5, '2018-05-29'),
(235, 12, 4, 1, '2018-05-30'),
(236, 12, 4, 1, '2018-05-31'),
(237, 17, 4, 1, '2018-05-01'),
(238, 17, 4, 1, '2018-05-02'),
(239, 17, 4, 1, '2018-05-03'),
(240, 17, 4, 3, '2018-05-04'),
(241, 17, 4, 4, '2018-05-05'),
(242, 17, 4, 5, '2018-05-06'),
(243, 17, 4, 1, '2018-05-07'),
(244, 17, 4, 1, '2018-05-08'),
(245, 17, 4, 2, '2018-05-09'),
(246, 17, 4, 3, '2018-05-10'),
(247, 17, 4, 4, '2018-05-11'),
(248, 17, 4, 5, '2018-05-12'),
(249, 17, 4, 5, '2018-05-13'),
(250, 17, 4, 1, '2018-05-14'),
(251, 17, 4, 1, '2018-05-15'),
(252, 17, 4, 5, '2018-05-16'),
(253, 17, 4, 2, '2018-05-17'),
(254, 17, 4, 3, '2018-05-18'),
(255, 17, 4, 4, '2018-05-19'),
(256, 17, 4, 5, '2018-05-20'),
(257, 17, 4, 5, '2018-05-21'),
(258, 17, 4, 1, '2018-05-22'),
(259, 17, 4, 2, '2018-05-23'),
(260, 17, 4, 3, '2018-05-24'),
(261, 17, 4, 4, '2018-05-25'),
(262, 17, 4, 5, '2018-05-26'),
(263, 17, 4, 1, '2018-05-27'),
(264, 17, 4, 1, '2018-05-28'),
(265, 17, 4, 1, '2018-05-29'),
(266, 17, 4, 3, '2018-05-30'),
(267, 17, 4, 4, '2018-05-31'),
(268, 27, 4, 5, '2018-05-01'),
(269, 27, 4, 1, '2018-05-02'),
(270, 27, 4, 2, '2018-05-03'),
(271, 27, 4, 3, '2018-05-04'),
(272, 27, 4, 4, '2018-05-05'),
(273, 27, 4, 5, '2018-05-06'),
(274, 27, 4, 3, '2018-05-07'),
(275, 27, 4, 4, '2018-05-08'),
(276, 27, 4, 5, '2018-05-09'),
(277, 27, 4, 1, '2018-05-10'),
(278, 27, 4, 1, '2018-05-11'),
(279, 27, 4, 1, '2018-05-12'),
(280, 27, 4, 2, '2018-05-13'),
(281, 27, 4, 2, '2018-05-14'),
(282, 27, 4, 3, '2018-05-15'),
(283, 27, 4, 4, '2018-05-16'),
(284, 27, 4, 5, '2018-05-17'),
(285, 27, 4, 2, '2018-05-18'),
(286, 27, 4, 5, '2018-05-19'),
(287, 27, 4, 3, '2018-05-20'),
(288, 27, 4, 4, '2018-05-21'),
(289, 27, 4, 5, '2018-05-22'),
(290, 27, 4, 5, '2018-05-23'),
(291, 27, 4, 1, '2018-05-24'),
(292, 27, 4, 2, '2018-05-25'),
(293, 27, 4, 2, '2018-05-26'),
(294, 27, 4, 3, '2018-05-27'),
(295, 27, 4, 4, '2018-05-28'),
(296, 27, 4, 1, '2018-05-29'),
(297, 27, 4, 2, '2018-05-30'),
(298, 27, 4, 2, '2018-05-31'),
(299, 18, 11, 5, '2018-05-01'),
(300, 18, 11, 1, '2018-05-02'),
(301, 18, 11, 1, '2018-05-03'),
(302, 18, 11, 1, '2018-05-04'),
(303, 18, 11, 1, '2018-05-05'),
(304, 18, 11, 5, '2018-05-06'),
(305, 18, 11, 1, '2018-05-07'),
(306, 18, 11, 1, '2018-05-08'),
(307, 18, 11, 1, '2018-05-09'),
(308, 18, 11, 5, '2018-05-10'),
(309, 18, 11, 1, '2018-05-11'),
(310, 18, 11, 1, '2018-05-12'),
(311, 18, 11, 5, '2018-05-13'),
(312, 18, 11, 1, '2018-05-14'),
(313, 18, 11, 1, '2018-05-15'),
(314, 18, 11, 1, '2018-05-16'),
(315, 18, 11, 1, '2018-05-17'),
(316, 18, 11, 1, '2018-05-18'),
(317, 18, 11, 1, '2018-05-19'),
(318, 18, 11, 5, '2018-05-20'),
(319, 18, 11, 1, '2018-05-21'),
(320, 18, 11, 1, '2018-05-22'),
(321, 18, 11, 1, '2018-05-23'),
(322, 18, 11, 1, '2018-05-24'),
(323, 18, 11, 1, '2018-05-25'),
(324, 18, 11, 1, '2018-05-26'),
(325, 18, 11, 5, '2018-05-27'),
(326, 18, 11, 1, '2018-05-28'),
(327, 18, 11, 5, '2018-05-29'),
(328, 18, 11, 1, '2018-05-30'),
(329, 18, 11, 1, '2018-05-31');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_kelamin`
--

CREATE TABLE `jenis_kelamin` (
  `jk` int(11) NOT NULL,
  `nama_jk` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_kelamin`
--

INSERT INTO `jenis_kelamin` (`jk`, `nama_jk`) VALUES
(1, 'L'),
(2, 'P');

-- --------------------------------------------------------

--
-- Table structure for table `laporan_bulanan`
--

CREATE TABLE `laporan_bulanan` (
  `id` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `bulan` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laporan_bulanan`
--

INSERT INTO `laporan_bulanan` (`id`, `id_nurse`, `judul`, `isi`, `bulan`) VALUES
(2, 10, 'bb', '<p>bbbbbbbbb</p>\r\n', '2018-04'),
(3, 10, 'Maret', 'Ini Laporan Bulan Maret', '2018-03');

-- --------------------------------------------------------

--
-- Table structure for table `laporan_harian`
--

CREATE TABLE `laporan_harian` (
  `id` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laporan_harian`
--

INSERT INTO `laporan_harian` (`id`, `id_nurse`, `judul`, `isi`, `tanggal`) VALUES
(8, 8, 'aaa', '<p>aaaaa</p>\r\n', '2018-04-09');

-- --------------------------------------------------------

--
-- Table structure for table `laporan_tahunan`
--

CREATE TABLE `laporan_tahunan` (
  `id` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `tahun` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laporan_tahunan`
--

INSERT INTO `laporan_tahunan` (`id`, `id_nurse`, `judul`, `isi`, `tahun`) VALUES
(1, 10, 'tahunan', '<p>tahun berjalans</p>\r\n', 2018),
(2, 9, 'Abdi teh Dini', '<p>Uhun teh dini</p>\r\n', 2018);

-- --------------------------------------------------------

--
-- Table structure for table `logbooks`
--

CREATE TABLE `logbooks` (
  `id` int(11) NOT NULL,
  `nurse_id` int(11) DEFAULT NULL,
  `pk_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `shift_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `patient_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logbooks`
--

INSERT INTO `logbooks` (`id`, `nurse_id`, `pk_id`, `activity_id`, `shift_id`, `date`, `time`, `patient_name`) VALUES
(40, 10, 4, 1, 2, '2018-05-01', '14:00:00', 'Tn. Amran'),
(41, 10, 4, 26, 2, '2018-05-01', '15:00:00', 'Tn Mirdad'),
(42, 10, 4, 0, 2, '2018-05-01', '16:00:00', ''),
(43, 10, 4, 0, 2, '2018-05-01', '17:00:00', ''),
(44, 10, 4, 0, 2, '2018-05-01', '18:00:00', ''),
(45, 10, 4, 0, 2, '2018-05-01', '19:00:00', ''),
(46, 10, 4, 0, 2, '2018-05-01', '20:00:00', ''),
(47, 10, 4, 2, 1, '2018-05-01', '07:00:00', 'Tn. Amran'),
(48, 10, 4, 25, 1, '2018-05-01', '08:00:00', 'Tn Mirdad dan Tn. Jaja'),
(49, 10, 4, 3, 1, '2018-05-01', '09:00:00', ''),
(50, 10, 4, 1, 1, '2018-05-01', '10:00:00', ''),
(51, 10, 4, 1, 1, '2018-05-01', '11:00:00', ''),
(52, 10, 4, 0, 1, '2018-05-01', '12:00:00', ''),
(53, 10, 4, 0, 1, '2018-05-01', '13:00:00', ''),
(54, 10, 4, 0, 1, '2018-05-01', '14:00:00', ''),
(55, 10, 4, 1, 0, '2018-05-02', '08:00:00', 'Tn. Jaka, Ket: Pasien ini menderita penyakit abc, dengan gejala awal blablabla'),
(56, 10, 4, 0, 0, '2018-05-02', '09:00:00', ''),
(57, 10, 4, 0, 0, '2018-05-02', '10:00:00', ''),
(58, 10, 4, 0, 0, '2018-05-02', '11:00:00', ''),
(59, 10, 4, 0, 0, '2018-05-02', '12:00:00', ''),
(60, 10, 4, 0, 0, '2018-05-02', '13:00:00', ''),
(61, 10, 4, 0, 0, '2018-05-02', '14:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `nurses`
--

CREATE TABLE `nurses` (
  `id_nurse` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `id_gol` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jk` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `id_status_kepegawaian` int(11) NOT NULL,
  `id_pk` int(11) NOT NULL,
  `id_pendidikan` int(11) NOT NULL,
  `id_jurusan` varchar(100) NOT NULL,
  `id_pelatihan` text NOT NULL,
  `gelar_depan` varchar(5) NOT NULL,
  `gelar_belakang` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL DEFAULT '0d7cb-hepdi.jpg',
  `password` varchar(50) NOT NULL DEFAULT 'password'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurses`
--

INSERT INTO `nurses` (`id_nurse`, `nama`, `nip`, `id_gol`, `id_jabatan`, `tanggal_lahir`, `jk`, `id_ruangan`, `id_status_kepegawaian`, `id_pk`, `id_pendidikan`, `id_jurusan`, `id_pelatihan`, `gelar_depan`, `gelar_belakang`, `image`, `password`) VALUES
(7, 'Osep Hernandi', '197101021996031005', 8, 1, '1971-01-02', 1, 9, 1, 4, 5, 'Master Keperawatan', 'BCTLS', 'H.', 'S.Sos., S.Kep., M.Kep', 'c01d5-osep-hernandi.jpg', 'hepdi'),
(9, 'Dini Andini', '197910082002122002', 4, 5, '1979-10-10', 2, 8, 1, 3, 2, 'Keperawatan', 'SMCC', '', '', '', 'password'),
(10, 'Aar Arisah', '196912071995032001', 8, 5, '1969-12-07', 2, 10, 1, 4, 4, 'Keperawatan', 'PMP', '', 'S.Kep, Ners', 'c13c4-aar-arisah.jpg', 'password'),
(11, 'Rifa Arsika Dewi', '197711132008012007', 6, 5, '1977-11-11', 2, 4, 1, 3, 4, 'Keperawatan', 'CCD', 'Hj.', 'S.Kep, Ners', 'a461b-rifa-arsika-dewi.jpg', 'password'),
(12, 'Ai Mulyati', '197903082008012004', 4, 7, '1979-03-08', 2, 4, 1, 3, 2, 'Keperawatan', 'SSC', '', '', 'a9bbf-ai-mulyati.jpg', 'password'),
(16, 'Daden Candradiningrat', '197604162007011005', 3, 6, '1976-04-16', 1, 10, 1, 2, 2, 'Keperawatan', '', '', 'AM. Kep', 'e1c4d-daden.jpg', 'password'),
(17, 'Dita Herdianti', '2018284', 0, 7, '1994-05-30', 2, 4, 2, 0, 4, 'Keperawatan', '', '', 'S.Kep, Ners', 'a07a1-dita-herdianti.jpg', 'password'),
(18, 'Santi Husna Taqiyah', '198202112007012003', 7, 6, '1982-02-11', 2, 11, 1, 3, 4, 'Keperawatan', '', '', 'Ners', '9e620-santi-husna.jpg', 'password'),
(19, 'Tating', '198204072008012003', 6, 7, '1982-04-07', 2, 11, 1, 3, 4, 'Keperawatan', '', '', 'S.Kep., Ners', 'b2945-tating.jpg', 'password'),
(20, 'Aditiana Nursukma', '676', 0, 7, '1989-01-28', 1, 11, 2, 0, 4, 'Keperawatan', '', '', 'S.Kep., Ners', '', 'password'),
(21, 'Ade Koadah', '196604201987032007', 0, 5, '1966-04-20', 2, 7, 1, 0, 2, 'Keperawatan', '', '', 'AM. Kep', '', 'password'),
(22, 'Niceu Hendriyani', '198012222007012003', 0, 6, '1980-12-22', 2, 7, 1, 0, 3, 'Kesehatan Masyarakat', '', '', 'S.KM', '', 'password'),
(23, 'Yulia', '197907102008012008', 0, 7, '1979-07-10', 2, 7, 1, 0, 2, 'Keperawatan', '', '', 'AM. Kep', '', 'password'),
(24, 'Wiwi Widyawati', '197903172007012007', 0, 7, '1979-03-17', 2, 7, 1, 0, 2, 'Keperawatan', '', '', 'AM. Kep', '', 'password'),
(25, 'Ika Mustikawati', '197711042007012009', 0, 5, '1977-11-04', 2, 5, 1, 0, 4, 'Keperawatan', '', '', 'S.Kep., Ners', '0a167-ika-mustikawati.jpg', 'password'),
(26, 'Martendi', '2013035', 0, 7, '1987-03-10', 1, 11, 2, 2, 2, 'Keperawatan', '', '', 'AM. Kep', '', 'password'),
(27, 'Bagas Ahmad Ibnu Tsulasi', '840', 0, 7, '1994-04-05', 1, 4, 3, 0, 4, 'Keperawatan', '', '', 'S.Kep.,Ners', 'a3f0a-bagas-ahmad.jpg', 'password'),
(28, 'Novi Novianti', '197811162000032002', 0, 7, '1978-11-16', 2, 5, 1, 0, 4, 'Keperawatan', '', '', 'S.Kep, Ners', '0c183-novi-novianti.jpg', 'password'),
(29, 'Tuti Suryati', '197711101999032005', 0, 5, '1977-11-10', 2, 13, 1, 0, 4, 'Keperawatan', '', 'Hj.', 'S.Kep., Ners', '99b80-tuti-suryati.jpg', 'password'),
(30, 'Popi Sri Muliawati', '198505112009122002', 0, 7, '1985-05-11', 2, 13, 1, 0, 4, 'Keperawatan', '', '', 'S.Kep, Ners', '', 'password'),
(31, 'Sopi Lasmi Hasanah', '2013092', 0, 7, '1987-06-15', 2, 13, 2, 0, 2, 'Keperawatan', '', '', 'AM. Kep', '', 'password'),
(32, 'Ayu Lisnawati', '834', 0, 7, '1991-03-25', 2, 13, 3, 0, 2, 'Keperawatan', '', '', 'AMd.Kep', '', 'password'),
(33, 'Nia Mayasari', '197502021998032000', 0, 5, '1975-02-02', 2, 14, 1, 0, 4, 'Keperawatan', '', '', 'S.Kep., Ners', '', 'password'),
(34, 'Ruri', '197502052002122001', 0, 7, '1975-02-05', 2, 14, 1, 0, 2, 'Keperawatan', '', '', 'AMK', '', 'password'),
(35, 'Sri Susilowati', '2013018', 0, 7, '1985-12-17', 2, 14, 2, 0, 2, '', '', '', 'A.Md.Kep', '', 'password'),
(36, 'Ricki Fauzi', '754', 0, 7, '0000-00-00', 0, 14, 3, 0, 0, '', '', '', '', '', 'password'),
(37, 'Djuwita Ratnasari', '198309162008012008', 5, 7, '1983-09-16', 2, 10, 1, 3, 4, 'Keperawatan', '', '', 'S.Kep, Ners', 'c7b8d-djuwita.jpg', 'password'),
(38, 'Restiani', '2017261', 0, 7, '1991-01-25', 2, 10, 2, 1, 2, 'Keperawatan', '', '', 'Amd.Kep', '5eedb-restiani.jpg', 'password'),
(39, 'Riza Farid Widiana', '763', 0, 7, '1995-11-27', 1, 10, 3, 5, 2, 'Keperawatan', '', '', 'Amd.Kep', '08c86-riza.jpg', 'password');

--
-- Triggers `nurses`
--
DELIMITER $$
CREATE TRIGGER `delete_simk` AFTER DELETE ON `nurses` FOR EACH ROW DELETE FROM simk WHERE simk.id_nurse = old.id_nurse
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `tambah_simk` AFTER INSERT ON `nurses` FOR EACH ROW INSERT INTO simk(id_simk,id_nurse,nira)
VALUES ('', NEW.id_nurse, '')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE `pendidikan` (
  `id_pendidikan` int(11) NOT NULL,
  `nama_pendidikan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan`
--

INSERT INTO `pendidikan` (`id_pendidikan`, `nama_pendidikan`) VALUES
(1, 'SPK'),
(2, 'DIII'),
(3, 'S1'),
(4, 'Ners'),
(5, 'S2');

-- --------------------------------------------------------

--
-- Table structure for table `pk_activity`
--

CREATE TABLE `pk_activity` (
  `activity_id` int(11) NOT NULL,
  `pk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pk_activity`
--

INSERT INTO `pk_activity` (`activity_id`, `pk_id`) VALUES
(1, 0),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3),
(4, 1),
(4, 2),
(4, 3),
(5, 1),
(5, 2),
(5, 3),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 2),
(7, 3),
(8, 1),
(8, 2),
(8, 3),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 2),
(10, 3),
(11, 1),
(11, 2),
(11, 3),
(11, 4),
(12, 1),
(12, 2),
(12, 3),
(12, 4),
(13, 1),
(13, 2),
(13, 3),
(13, 4),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 2),
(26, 3),
(26, 4),
(27, 2),
(28, 2),
(29, 2),
(30, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 3),
(41, 4),
(42, 3),
(42, 4),
(43, 3),
(43, 4),
(44, 3),
(44, 4),
(45, 3),
(45, 4),
(46, 3),
(46, 4),
(47, 3),
(47, 4),
(48, 3),
(48, 4),
(49, 3),
(49, 4),
(50, 3),
(50, 4),
(51, 3),
(51, 4),
(52, 3),
(52, 4),
(53, 3),
(53, 4),
(54, 3),
(54, 4),
(55, 4),
(56, 4),
(57, 4),
(58, 4),
(59, 4),
(60, 4),
(61, 4),
(62, 4),
(63, 4),
(64, 4),
(65, 4),
(66, 4),
(67, 4),
(68, 4),
(69, 4),
(70, 4);

-- --------------------------------------------------------

--
-- Table structure for table `rencana_kegiatan_bulanan`
--

CREATE TABLE `rencana_kegiatan_bulanan` (
  `id_rencana_bulanan` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `jenis_kegiatan` varchar(100) NOT NULL,
  `rencana_anggaran` int(100) NOT NULL,
  `bulan` date NOT NULL,
  `rincian` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rencana_kegiatan_bulanan`
--

INSERT INTO `rencana_kegiatan_bulanan` (`id_rencana_bulanan`, `id_nurse`, `id_ruangan`, `jenis_kegiatan`, `rencana_anggaran`, `bulan`, `rincian`) VALUES
(9, 10, 4, 'Pensiun', 15000000, '2018-01-01', '<p>ssss</p>\r\n'),
(10, 10, 4, 'Cuci Tangan Memakai Tanah Liat', 100000000, '2018-06-01', '<p>1. Mencuci tangan memakai tanah liat yang sudah dikeringkan</p>\r\n\r\n<p>2. Kemudian dikeringkan di bawah terik panas matahari</p>\r\n\r\n<p>3. Wew</p>\r\n'),
(11, 9, 8, 'Pembersian Alat Tangkap', 500000, '2018-07-01', '<p>Membersihkan peralatang untuk menangkap ikan</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `rencana_kegiatan_tahunan`
--

CREATE TABLE `rencana_kegiatan_tahunan` (
  `id_rencana_tahunan` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `jenis_kegiatan` varchar(100) NOT NULL,
  `rencana_anggaran` int(100) NOT NULL,
  `tahun` year(4) NOT NULL,
  `rincian` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rencana_kegiatan_tahunan`
--

INSERT INTO `rencana_kegiatan_tahunan` (`id_rencana_tahunan`, `id_nurse`, `id_ruangan`, `jenis_kegiatan`, `rencana_anggaran`, `tahun`, `rincian`) VALUES
(1, 10, 4, 'Dekorasi Pantry', 2000000, 2019, '<p>Rencana Kegiatan Mendekorasi Pantry Ruangan</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `ruangan`
--

CREATE TABLE `ruangan` (
  `id_ruangan` int(11) NOT NULL,
  `nama_ruangan` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruangan`
--

INSERT INTO `ruangan` (`id_ruangan`, `nama_ruangan`) VALUES
(1, 'Melati'),
(2, 'Mawar'),
(3, 'ICU'),
(4, 'IGD'),
(5, 'Dahlia I'),
(6, 'Kenanga'),
(7, 'VIP'),
(8, 'Thalasemia'),
(9, 'Bidang Keperawatan'),
(10, 'Bougenvile Lt. I'),
(11, 'Wijaya Kusumah II'),
(13, 'NICU'),
(14, 'Wijaya Kusumah I'),
(17, 'Dahlia II'),
(18, 'Bougenvile Lt. 2'),
(19, 'Rawat Jalan');

-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE `shift` (
  `id_shift` int(11) NOT NULL,
  `nama_shift` varchar(30) NOT NULL,
  `jam_masuk` time DEFAULT NULL,
  `jam_pulang` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shift`
--

INSERT INTO `shift` (`id_shift`, `nama_shift`, `jam_masuk`, `jam_pulang`) VALUES
(1, 'P', '07:00:00', '14:00:00'),
(2, 'S', '14:00:00', '20:00:00'),
(3, 'M', '20:00:00', '07:00:00'),
(4, 'L/M', NULL, NULL),
(5, 'L', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `simk`
--

CREATE TABLE `simk` (
  `id_simk` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `nira` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `simk`
--

INSERT INTO `simk` (`id_simk`, `id_nurse`, `nira`) VALUES
(4, 7, '32070026229'),
(6, 9, '32070271437'),
(7, 10, '32070026581'),
(8, 11, '32070030214'),
(9, 12, '32070026224'),
(10, 16, ''),
(11, 17, ''),
(12, 18, '32070271478'),
(13, 19, ''),
(14, 20, ''),
(15, 21, ''),
(16, 22, ''),
(17, 23, ''),
(18, 24, ''),
(19, 25, ''),
(20, 26, ''),
(21, 27, ''),
(22, 28, ''),
(23, 29, ''),
(24, 30, ''),
(25, 31, ''),
(26, 32, ''),
(27, 33, ''),
(28, 34, ''),
(29, 35, ''),
(30, 36, ''),
(31, 37, ''),
(32, 38, ''),
(33, 39, '');

-- --------------------------------------------------------

--
-- Table structure for table `status_kepegawaian`
--

CREATE TABLE `status_kepegawaian` (
  `id_status_kepegawaian` int(11) NOT NULL,
  `status_kepegawaian` varchar(50) NOT NULL,
  `keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_kepegawaian`
--

INSERT INTO `status_kepegawaian` (`id_status_kepegawaian`, `status_kepegawaian`, `keterangan`) VALUES
(1, 'PNS', 'Pegwai Negeri Sipil'),
(2, 'TKK', 'Tenaga Kerja Kontrak'),
(3, 'Magang', 'Magang');

-- --------------------------------------------------------

--
-- Table structure for table `tugas_pokok`
--

CREATE TABLE `tugas_pokok` (
  `id_pk` int(11) NOT NULL,
  `nama_pk` varchar(6) NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tugas_pokok`
--

INSERT INTO `tugas_pokok` (`id_pk`, `nama_pk`, `keterangan`) VALUES
(1, 'PK 1', ''),
(2, 'PK 2', ''),
(3, 'PK 3', ''),
(4, 'PK 4', ''),
(5, 'Pra PK', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `id_nurse` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `id_nurse`, `username`, `password`) VALUES
(1, 0, 'admin', 'admin'),
(2, 7, '8766500', '8766500');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `golongan`
--
ALTER TABLE `golongan`
  ADD PRIMARY KEY (`id_gol`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indexes for table `jenis_kelamin`
--
ALTER TABLE `jenis_kelamin`
  ADD PRIMARY KEY (`jk`);

--
-- Indexes for table `laporan_bulanan`
--
ALTER TABLE `laporan_bulanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan_harian`
--
ALTER TABLE `laporan_harian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan_tahunan`
--
ALTER TABLE `laporan_tahunan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logbooks`
--
ALTER TABLE `logbooks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurses`
--
ALTER TABLE `nurses`
  ADD PRIMARY KEY (`id_nurse`);

--
-- Indexes for table `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD PRIMARY KEY (`id_pendidikan`);

--
-- Indexes for table `pk_activity`
--
ALTER TABLE `pk_activity`
  ADD PRIMARY KEY (`activity_id`,`pk_id`);

--
-- Indexes for table `rencana_kegiatan_bulanan`
--
ALTER TABLE `rencana_kegiatan_bulanan`
  ADD PRIMARY KEY (`id_rencana_bulanan`);

--
-- Indexes for table `rencana_kegiatan_tahunan`
--
ALTER TABLE `rencana_kegiatan_tahunan`
  ADD PRIMARY KEY (`id_rencana_tahunan`);

--
-- Indexes for table `ruangan`
--
ALTER TABLE `ruangan`
  ADD PRIMARY KEY (`id_ruangan`);

--
-- Indexes for table `shift`
--
ALTER TABLE `shift`
  ADD PRIMARY KEY (`id_shift`);

--
-- Indexes for table `simk`
--
ALTER TABLE `simk`
  ADD PRIMARY KEY (`id_simk`);

--
-- Indexes for table `status_kepegawaian`
--
ALTER TABLE `status_kepegawaian`
  ADD PRIMARY KEY (`id_status_kepegawaian`);

--
-- Indexes for table `tugas_pokok`
--
ALTER TABLE `tugas_pokok`
  ADD PRIMARY KEY (`id_pk`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `golongan`
--
ALTER TABLE `golongan`
  MODIFY `id_gol` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=330;

--
-- AUTO_INCREMENT for table `jenis_kelamin`
--
ALTER TABLE `jenis_kelamin`
  MODIFY `jk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `laporan_bulanan`
--
ALTER TABLE `laporan_bulanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `laporan_harian`
--
ALTER TABLE `laporan_harian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `laporan_tahunan`
--
ALTER TABLE `laporan_tahunan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `logbooks`
--
ALTER TABLE `logbooks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `nurses`
--
ALTER TABLE `nurses`
  MODIFY `id_nurse` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `pendidikan`
--
ALTER TABLE `pendidikan`
  MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `rencana_kegiatan_bulanan`
--
ALTER TABLE `rencana_kegiatan_bulanan`
  MODIFY `id_rencana_bulanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `rencana_kegiatan_tahunan`
--
ALTER TABLE `rencana_kegiatan_tahunan`
  MODIFY `id_rencana_tahunan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ruangan`
--
ALTER TABLE `ruangan`
  MODIFY `id_ruangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `shift`
--
ALTER TABLE `shift`
  MODIFY `id_shift` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `simk`
--
ALTER TABLE `simk`
  MODIFY `id_simk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `status_kepegawaian`
--
ALTER TABLE `status_kepegawaian`
  MODIFY `id_status_kepegawaian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tugas_pokok`
--
ALTER TABLE `tugas_pokok`
  MODIFY `id_pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
