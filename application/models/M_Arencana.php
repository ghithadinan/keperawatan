<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Arencana extends CI_Model{
	function ruangan(){
        $hasil=$this->db->query("SELECT * FROM ruangan");
        return $hasil;
    }
	/*public function bulana($id,$jk){
		$this->db->select('nama,nama_jabatan,nira,nurses.id_nurse,nama_ruangan,jenis_kelamin.nama_jk');
		$this->db->from('nurses');
		$this->db->join('jabatan', 'nurses.id_jabatan = jabatan.id_jabatan');
		$this->db->join('simk', 'nurses.id_nurse = simk.id_nurse');
		$this->db->join('ruangan', 'nurses.id_ruangan = ruangan.id_ruangan');	
		$this->db->join('jenis_kelamin','nurses.jk = jenis_kelamin.jk');	
		if($id==='a'AND $jk==='a'){
			$query = $this->db->get();
			return $query->result();
		}elseif($id==='a' AND $jk!='a'){
		$this->db->where('nurses.jk',$jk);	
		$query = $this->db->get();
		return $query->result();
		}elseif($id!='a' AND $jk==='a'){
		$this->db->where('nurses.id_ruangan',$id);
		$query = $this->db->get();
		return $query->result();
		}else{
			$this->db->where('nurses.id_ruangan',$id);
			$this->db->where('nurses.jk',$jk);
			$query = $this->db->get();
			return $query->result();
		}	
	}*/

	public function bulanans(){
		$this->db->select('ruangan.nama_ruangan,jenis_kegiatan,rencana_anggaran,bulan,rincian');
		$this->db->from('rencana_kegiatan_bulanan');
		$this->db->join('ruangan', 'rencana_kegiatan_bulanan.id_ruangan = ruangan.id_ruangan');	
		$query = $this->db->get();
		return $query;
	}

	public function tampil_bul($th,$bl,$id){
		$date=$th."-".$bl."-01";
		$t=date('Y-m-d', strtotime($date));
		$this->db->select('ruangan.nama_ruangan,jenis_kegiatan,rencana_anggaran,bulan,rincian');
		$this->db->from('rencana_kegiatan_bulanan');
		$this->db->join('ruangan', 'rencana_kegiatan_bulanan.id_ruangan = ruangan.id_ruangan');
	
		if($bl==='a'){
			$this->db->where('ruangan.id_ruangan',$id);
			$query = $this->db->get();
			return $query->result();
		}else{
			
			$this->db->where('rencana_kegiatan_bulanan.bulan',$t);
			$this->db->where('ruangan.id_ruangan',$id);
			$query = $this->db->get();
			return $query->result();
		}	
	}	
	
}
