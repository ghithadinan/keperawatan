<?php

class M_Activity extends CI_Model {

	protected $tables = 'activities';

	public function getData()
	{
		$this->db->from($this->tables);
		return $this->db->get();
	}

	public function getById($id)
    {
        $this->db->from($this->tables);
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function getByPk($pkId)
    {
    	$this->db->select('ac.id as id, ac.name as name, ac.id as ac_id,
							ac.`name` as activity_name,
							pk.id_pk as pk_id,
							pk.nama_pk as pk_name');

        $this->db->from($this->tables.' as ac');
        $this->db->join('pk_activity as pka', 'pka.activity_id = ac.id');
        $this->db->join('tugas_pokok as pk', 'pk.id_pk = pka.pk_id');
        $this->db->where('pk.id_pk', $pkId);

        return $this->db->get();
    }

    public function getPk($pkId, $acId)
    {
        $this->db->from('pk_activity');
        $this->db->where('activity_id', $acId);
        $this->db->where('pk_id', $pkId);
        return $this->db->get();
    }

    public function createByPk($pkId, $acId)
    {
        return $this->db->insert('pk_activity', ['activity_id' => $acId, 'pk_id' => $pkId]);
    }

    public function deleteByPk($pkId, $acId)
    {
        return $this->db->delete('pk_activity', array('pk_id' => $pkId, 'activity_id' => $acId));
    }
}
