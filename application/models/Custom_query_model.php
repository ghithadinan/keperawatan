<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Custom_query_model extends CI_Model {
 
	function __construct() {
		parent::__construct();
	}
 
	function get_list() {
		$query=$this->db->query('SELECT simk.id_simk,simk.nira,nurses.id_nurse,nurses.nip,nurses.nama, ruangan.nama_ruangan FROM simk RIGHT JOIN nurses ON simk.id_nurse = nurses.id_nurse INNER JOIN ruangan ON nurses.id_ruangan=ruangan.id_ruangan');
 
		$results_array=$query->result();
		return $results_array;		
	}
	
	function update_nira($id,$nira) {
		$this->db->set('nira',$nira);
		$this->db->where('id_simk',$id);
        $this->db->update('simk');	
	}
}