<?php
	class M_Profile extends CI_Model{

		function perawat_ruangan(){
        $hasil=$this->db->query("SELECT * FROM ruangan");
        return $hasil;
    }
    	function golongan_ruang(){
    		$hasil=$this->db->query("SELECT * FROM golongan");
        	return $hasil;
    	}

    	function pk_ruang(){
    		$hasil=$this->db->query("SELECT * FROM tugas_pokok");
        	return $hasil;
    	}
		function get_perawat($id){
    		$hasil=$this->db->query("SELECT * FROM nurses WHERE id_ruangan='$id'");
        	return $hasil->result();
    	}

        function get_data_individu($id){
            $this->db->select('nama,tanggal_lahir,nip,golongan.nama_gol,golongan.pangkat,nama_jabatan,nira,nurses.id_nurse,nurses.id_ruangan,ruangan.nama_ruangan,status_kepegawaian,nama_pk,jk,gelar_depan,gelar_belakang,image');
            $this->db->from('nurses');
            $this->db->join('jabatan', 'nurses.id_jabatan = jabatan.id_jabatan');
            $this->db->join('golongan', 'nurses.id_gol = golongan.id_gol');
            $this->db->join('simk', 'nurses.id_nurse = simk.id_nurse');
            $this->db->join('ruangan', 'nurses.id_ruangan = ruangan.id_ruangan');
            $this->db->join('status_kepegawaian', 'nurses.id_status_kepegawaian = status_kepegawaian.id_status_kepegawaian');
            $this->db->join('tugas_pokok', 'nurses.id_pk = tugas_pokok.id_pk');
            $this->db->where('nurses.id_nurse', $id);
            $query = $this->db->get();
            return $query->result();
        }
	}