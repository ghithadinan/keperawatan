<?php

class M_Logbook extends CI_Model {

	protected $tables = 'logbooks';

	public function getData($filter = NULL, $relation = NULL)
	{
		$this->db->from($this->tables);
		if(!empty($filter) && is_array($filter)) {
			$this->db->where($filter);
		}

		return $this->db->get();
	}

    public function getByPk($pkId)
    {
    	$this->db->select('ac.id as ac_id,
							ac.`name` as activity_name,
							pk.id_pk as pk_id,
							pk.nama_pk as pk_name');

        $this->db->from($this->tables.' as ac');
        $this->db->join('pk_activity as pka', 'pka.activity_id = ac.id');
        $this->db->join('tugas_pokok as pk', 'pk.id_pk = pka.pk_id');
        $this->db->where('pk.id_pk', $pkId);

        return $this->db->get();
    }

    public function getDataByMonth($month, $filter = NULL)
    {
        $this->db->from($this->tables);
        if(!empty($filter) && is_array($filter)) {
            $this->db->where($filter);
        }

        $this->db->where("DATE_FORMAT(date,'%Y-%m') =", $month);

        return $this->db->get();
    }

    public function createData($data)
    {
    	return $this->db->insert($this->tables, $data);
    }

    public function updateData($cond, $data)
    {
    	if(is_array($cond)) {
	    	$this->db->where($cond);
    	} else {
    		$this->db->where('id', $cond);
    	}

    	return $this->db->update($this->tables, $data);
    }
}
