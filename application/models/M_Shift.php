<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_Shift
 *
 * @author Gets
 */
class M_Shift extends CI_Model {

    public function getAll() {
        $this->db->from("shift");
        $this->db->order_by('id_shift', 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function getById($id) {
        $this->db->from('shift');
        $this->db->where('id_shift', $id);
        return $this->db->get()->row();
    }

}
