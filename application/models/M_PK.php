<?php

class M_PK extends CI_Model {

	protected $tables = 'tugas_pokok';

	public function getData()
	{
		$this->db->from($this->tables);
		return $this->db->get();
	}

	public function getById($id)
    {
        $this->db->from($this->tables);
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }
}
