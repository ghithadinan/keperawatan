<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_Nurses
 *
 * @author Gets
 */
class M_Nurses extends CI_Model {

    public function getByRoom($roomId) {
        $this->db->from('nurses');
        $this->db->where('id_ruangan', $roomId);

        return $this->db;
    }

    public function getById($id) {
        $this->db->from('nurses');
        $this->db->where('id_nurse', $id);
        return $this->db->get()->row();
    }

    public function getByIdWithRelation($id, $relation = []) {
        $this->db->from('nurses');
        $this->db->where('id_nurse', $id);

        $data = $this->db->get()->row_array();

        foreach ($relation as $key => $value) {
            switch ($value) {
                case 'room':
                    $relation = $this->db->from('ruangan')->where('id_ruangan', $data['id_ruangan'])->get()->row();

                    if ($relation)
                        $data = array_merge($data, ['room' => $relation]);
                    break;
                case 'pk':
                    $relation = $this->db->from('tugas_pokok')->where('id_pk', $data['id_pk'])->get()->row();
                    if ($relation)
                        $data = array_merge($data, ['pk' => $relation]);
                    break;
                default:
                    # code...
                    break;
            }
        }

        return (Object) $data;
    }

    public function getListBy($where = []) {
        $this->db->from('nurses');
        $this->db->where($where);
        $this->db->join('jabatan', 'nurses.id_jabatan=jabatan.id_jabatan', 'left');
        $this->db->join('pendidikan', 'nurses.id_pendidikan=pendidikan.id_pendidikan', 'left');
        $this->db->order_by('nurses.id_jabatan', 'asc');
        return $this->db->get()->result();
    }

    public function getAll() {
        $this->db->from('nurses');
        $this->db->join('jabatan', 'nurses.id_jabatan=jabatan.id_jabatan', 'left');
        $this->db->join('pendidikan', 'nurses.id_pendidikan=pendidikan.id_pendidikan', 'left');
        $this->db->order_by('nurses.id_jabatan', 'asc');
        return $this->db->get()->result();
    }

}
