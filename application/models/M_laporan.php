<?php
	class M_laporan extends CI_Model{

		public function lharian(){
			$datestring = "%Y-%m-%d";
			$t=mdate($datestring);
		$data=array(
			'id_nurse'=>$this->session->userdata('user_id'),
			'judul'=>$this->input->post('judul'),
			'isi'=>$this->input->post('isi'),
			'tanggal'=>$t
			);
		return $this->db->insert('laporan_harian',$data);
		}
		
		public function ltanggal(){
		$id=$this->session->userdata('user_id');
		$this->db->select('tanggal');
		$this->db->from('laporan_harian');
		$this->db->where('id_nurse', $id);
		$query = $this->db->get();
		if($this->db->count_all_results()>0){
		return $query->result();
		}else{
			return false;
		}
		}

		public function lbulanan(){
			$datestring = "%Y-%m";
			$t=mdate($datestring);
		$data=array(
			'id_nurse'=>$this->session->userdata('user_id'),
			'judul'=>$this->input->post('judul'),
			'isi'=>$this->input->post('isi'),
			'bulan'=>$t
			);
		return $this->db->insert('laporan_bulanan',$data);
		}
		
		public function lbulan(){
		$id=$this->session->userdata('user_id');
		$this->db->select('bulan');
		$this->db->from('laporan_bulanan');
		$this->db->where('id_nurse', $id);
		$query = $this->db->get();
		if($this->db->count_all_results()>0){
		return $query->result();
		}else{
			return false;
		}
		}

		public function ltahunan(){
			$datestring = "%Y";
			$t=mdate($datestring);
		$data=array(
			'id_nurse'=>$this->session->userdata('user_id'),
			'judul'=>$this->input->post('judul'),
			'isi'=>$this->input->post('isi'),
			'tahun'=>$t
			);
		return $this->db->insert('laporan_tahunan',$data);
		}
		
		public function ltahun(){
		$id=$this->session->userdata('user_id');
		$this->db->select('tahun');
		$this->db->from('laporan_tahunan');
		$this->db->where('id_nurse', $id);
		$query = $this->db->get();
		if($this->db->count_all_results()>0){
		return $query->result();
		}else{
			return false;
		}
		}
	}

	