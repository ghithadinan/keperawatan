<?php

class M_Ruangan extends CI_Model {

    protected $tables = 'ruangan';

    public function getData() {
        $this->db->from($this->tables);
        return $this->db->get();
    }

    public function getById($id) {
        $this->db->from($this->tables);
        $this->db->where('id_ruangan', $id);
        return $this->db->get()->row();
    }

}
