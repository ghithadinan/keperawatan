<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_Cetak
 *
 * @author Heps
 */
class M_Cetak extends CI_Model {

    public function getAll() {
        $this->db->select('nurses.nip,nurses.nama,golongan.nama_gol');
        $this->db->from('nurses');
        $this->db->join('golongan','nurses.id_gol=golongan.id_gol');
        $this->db->order_by('nurses.id_gol', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function getById($id) {
        $this->db->from('shift');
        $this->db->where('id_shift', $id);
        return $this->db->get();
    }

        public function getPK() {
        $this->db->select('nurses.nip,nurses.nama,tugas_pokok.nama_pk');
        $this->db->from('nurses');
        $this->db->join('tugas_pokok','nurses.id_pk=tugas_pokok.id_pk','left');
        $this->db->order_by('nurses.id_pk', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

}
