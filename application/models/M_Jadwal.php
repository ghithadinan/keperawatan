<?php

class M_Jadwal extends CI_Model {

    public function tampil() {
        $idRuangan = $this->session->userdata('id_ruangan');
        $hasil = $this->db->query("SELECT * FROM nurses where id_ruangan=$idRuangan");
        return $hasil->result();
    }

    public function tampil_jadwal() {
        $this->db->select("id_jadwal, id_nurse, id_ruangan, jadwal.id_shift, nama_shift, jam_masuk, jam_pulang, tgl_jadwal");
        $this->db->from("jadwal");
        $this->db->join("shift", "jadwal.id_shift = shift.id_shift");
        $query = $this->db->get();
        return $query->result();
    }

    public function getListByRuanganNurseId($idRuangan, $nurseId) {
        $this->db->from("jadwal");
        $this->db->where([
            'id_ruangan' => $idRuangan,
            'id_nurse' => $nurseId
        ]);
        $this->db->order_by('id_jadwal', 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function getByNurseRuanganTglJadwal($nurseId, $idRuangan, $tglJadwal) {
        $this->db->from("jadwal");
        $this->db->where([
            'id_nurse' => $nurseId,
            'id_ruangan' => $idRuangan,
            'tgl_jadwal' => $tglJadwal
        ]);
        return $this->db->get()->row();
    }

    public function create($data = array()) {
        $this->db->insert('jadwal', $data);
        $id = $this->db->insert_id();
        return $id;
    }

    public function update($where = array(), $data = array()) {
        $this->db->where($where);
        $query = $this->db->update('jadwal', $data);
        return $query;
    }

    public function delete($where = array()) {
        $query = $this->db->delete('jadwal', $where);
        return $query;
    }

    public function tampilJadwal() {
        $hasil = $this->db->query("SELECT * FROM ruangan order by nama_ruangan asc");
        return $hasil->result();
    }

    public function getShiftBy($where = []) {
        $this->db->select('shift.*');
        $this->db->from('jadwal');
        $this->db->where($where);
        $this->db->join('shift', 'shift.id_shift=jadwal.id_shift', 'left');
        return $this->db->get()->row();
    }

}
