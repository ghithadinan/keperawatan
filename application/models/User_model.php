<?php

class User_model extends CI_Model {

    public function login($username, $password) {
        $this->db->select('nurses.nama,nurses.image,jabatan.nama_jabatan,nurses.id_jabatan,nira,nurses.id_nurse,nurses.id_ruangan,id_pk,nama_ruangan');
        $this->db->from('nurses');
        $this->db->join('jabatan', 'nurses.id_jabatan = jabatan.id_jabatan');
        $this->db->join('simk', 'nurses.id_nurse = simk.id_nurse');
        $this->db->join('ruangan', 'nurses.id_ruangan = ruangan.id_ruangan');
        $this->db->where('nira', $username);
        $this->db->where('password', $password);

        $query = $this->db->get();
        return $query->result();
    }

}
