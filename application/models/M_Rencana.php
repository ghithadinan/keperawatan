<?php
	class M_Rencana extends CI_Model{

		public function input_bulanan(){
			$a=$this->input->post('anggaran');
			$angka= str_replace(".", "", $a);
			$Y=$this->input->post('tahun');
			$m=$this->input->post('bulan');
			$date=$Y."-".$m."-01";
			$t=date('Y-m-d', strtotime($date));
			
		$data=array(
			'id_nurse'=>$this->session->userdata('user_id'),
			'id_ruangan'=>$this->session->userdata('id_ruangan'),
			'jenis_kegiatan'=>$this->input->post('jenis_kegiatan'),
			'rencana_anggaran'=>$angka,
			'bulan'=>$t,
			'rincian'=>$this->input->post('isi')
			);
		return $this->db->insert('rencana_kegiatan_bulanan',$data);
		}
		
		public function input_tahunan(){
			$a=$this->input->post('anggaran');
			$angka= str_replace(".", "", $a);
	
		$data=array(
			'id_nurse'=>$this->session->userdata('user_id'),
			'id_ruangan'=>$this->session->userdata('id_ruangan'),
			'jenis_kegiatan'=>$this->input->post('jenis_kegiatan'),
			'rencana_anggaran'=>$angka,
			'tahun'=>$this->input->post('tahun'),
			'rincian'=>$this->input->post('isi')
			);
		return $this->db->insert('rencana_kegiatan_tahunan',$data);
		}

	}

	