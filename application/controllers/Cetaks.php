<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetaks extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}
	public function index()
	{
		$this->load->model('M_Cetak');
		$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('nurses');
			$output = $crud->render();
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$data['x']=$this->M_Cetak->getAll();
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('print/index.php',$data);
		$this->load->view('layout/footer.php');
	}

	public function perawat()
	{


        $data=$this->M_Print->get_perawat();
  
	}

	public function kualifikasi()
	{
		$this->load->model('M_Cetak');
		$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('nurses');
			$output = $crud->render();
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$data['x']=$this->M_Cetak->getPK();
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('print/kualifikasi.php',$data);
		$this->load->view('layout/footer.php');
	}


}
