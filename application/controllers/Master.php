<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}
	public function index()
	{
		$this->read_golongan((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function golongan()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('golongan');
			$crud->set_subject('Golongan');
			$crud->set_language('indonesian');
			$crud->unset_clone();
			$crud->required_fields('nama_golongan');

			$output = $crud->render();

			$this->read_golongan($output);
	}

	public function read_golongan($output = null)
	{	if(!$this->session->userdata('logged_in')){
			redirect('users/index');
		}
		$data['judul']='GOLONGAN';
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('golongan.php',$data);
		$this->load->view('layout/footer.php');
	}

	public function jabatan()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('jabatan');
			$crud->set_subject('Jabatan');
			$crud->set_language('indonesian');
			$crud->unset_clone();
			$crud->required_fields('nama_jabatan');


			$output = $crud->render();

			$this->read_jabatan($output);
	}

	public function read_jabatan($output = null)
	{
		if(!$this->session->userdata('logged_in')){
			redirect('users/index');
		}
		$data['judul']='JABATAN';
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('jabatan.php',$data);
		$this->load->view('layout/footer.php');
	}

	public function ruangan()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('ruangan');
			$crud->set_subject('Ruangan');
			$crud->set_language('indonesian');
			$crud->unset_clone();
			$crud->required_fields('nama_ruangan');


			$output = $crud->render();

			$this->read_ruangan($output);
	}

	public function read_ruangan($output = null)
	{
		if(!$this->session->userdata('logged_in')){
			redirect('users/index');
		}
		$data['judul']='RUANGAN';
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('ruangan.php',$data);
		$this->load->view('layout/footer.php');
	}

	public function pendidikan()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('pendidikan');
			$crud->set_subject('Pendidikan');
			$crud->set_language('indonesian');
			$crud->unset_clone();
			$crud->required_fields('nama_pendidikan');


			$output = $crud->render();

			$this->read_pendidikan($output);
	}

	public function read_pendidikan($output = null)
	{
		if(!$this->session->userdata('logged_in')){
			redirect('users/index');
		}
		$data['judul']='PENDIDIKAN';
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('pendidikan.php',$data);
		$this->load->view('layout/footer.php');
	}

	public function activity()
	{
		$crud = new grocery_CRUD();

		$crud->set_theme('datatables');
		$crud->set_table('activities');
		$crud->set_subject('Kegiatan');
		$crud->set_language('indonesian');
		$crud->unset_clone();
		$crud->required_fields('name');

		$output = $crud->render();

		$this->read_activity($output);
	}

	public function read_activity($output = null)
	{
		if(!$this->session->userdata('logged_in'))
		{
			redirect('users/index');
		}

		$data['judul']='PENDIDIKAN';
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('activity/index', $data);
		$this->load->view('layout/footer.php');
	}

	public function activityPk()
	{
		if(!$this->session->userdata('logged_in')){
			redirect('users/index');
		}

		$this->load->model('M_PK');
		$this->load->model('M_Activity');

		$data['pk'] = $this->M_PK->getData()->result();
		$data['activities'] = $this->M_Activity->getData()->result();

		$this->load->view('layout/header.php');
		$this->load->view('pk/index.php', $data);
		$this->load->view('layout/footer.php');
	}

	public function activityPkDetail()
	{
		$this->load->model('M_Activity');

		$pkId = $this->input->get('pk_id');
		$listData = $this->M_Activity->getByPk($pkId)->result();

		$data['listData'] = $listData;

		header('Content-Type: application/json');
		echo json_encode($data); die();
	}

	public function activityPkDetailCreate()
	{
		$this->load->model('M_Activity');

		$pkId = $this->input->post('pk_id');
		$acId = $this->input->post('activity_id');

		if(is_array($acId)) {
			foreach ($acId as $key => $value) {
				$check = $this->M_Activity->getPk($pkId, $value)->num_rows();

				if(! $check) {
					$this->M_Activity->createByPk($pkId, $value);					
				}
			}
		} else {
			$this->M_Activity->createByPk($pkId, $acId);
		}

		header('Content-Type: application/json');
		echo json_encode(['message' => 'Success']); die();
	}

	public function activityPkDetailDelete()
	{
		$this->load->model('M_Activity');

		$pkId = $this->input->post('pk_id');
		$acId = $this->input->post('ac_id');

		$delete = $this->M_Activity->deleteByPk($pkId, $acId);

		header('Content-Type: application/json');
		echo json_encode($delete); die();
	}
}
