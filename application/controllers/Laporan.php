<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
	}

public function harian(){
	$tang=$this->M_laporan->ltanggal();
	$l=FALSE;
	foreach($tang as $tgl) {
		$l=$tgl->tanggal;
	}
	$datestring = "%Y-%m-%d";
	$tg=mdate($datestring);
	if($l===$tg){
		$crud = new grocery_CRUD();
		$crud->set_table('nurses');
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$output = $crud->render();
		$data['nama'] = $this->session->userdata('nama');
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('lada.php',$data);
		$this->load->view('layout/footer.php',(array)$output);
	}else{
	$crud = new grocery_CRUD();
	$crud->set_table('nurses');
	((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	$output = $crud->render();
	$data['nama'] = $this->session->userdata('nama');
	$this->load->view('layout/header.php',(array)$output);
	$this->load->view('lharian.php',$data);
	$this->load->view('layout/footer.php',(array)$output);
	}
}

public function input_harian(){
	$this->form_validation->set_rules('judul','judul', 'required');
	$this->form_validation->set_rules('isi','isi', 'required');
	if($this->form_validation->run() === FALSE){
		$crud = new grocery_CRUD();
		$crud->set_table('nurses');
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$output = $crud->render();
		$data['nama'] = $this->session->userdata('nama');
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('lharian.php',$data);
		$this->load->view('layout/footer.php',(array)$output);
	}else{
		$this->M_laporan->lharian();
		redirect('perawat/index');
	}
}

public function bulanan(){
	$tang=$this->M_laporan->lbulan();
	$l=FALSE;
	foreach($tang as $tgl) {
		$l=$tgl->bulan;
	}
	$datestring = "%Y-%m";
	$tg=mdate($datestring);
	if($l===$tg){
		$crud = new grocery_CRUD();
		$crud->set_table('nurses');
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$output = $crud->render();
		$data['nama'] = $this->session->userdata('nama');
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('lala.php',$data);
		$this->load->view('layout/footer.php',(array)$output);
	}else{
	$crud = new grocery_CRUD();
	$crud->set_table('nurses');
	((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	$output = $crud->render();
	$data['nama'] = $this->session->userdata('nama');
	$this->load->view('layout/header.php',(array)$output);
	$this->load->view('lbulanan.php',$data);
	$this->load->view('layout/footer.php',(array)$output);
	}
}

public function input_bulanan(){
	$this->form_validation->set_rules('judul','judul', 'required');
	$this->form_validation->set_rules('isi','isi', 'required');
	if($this->form_validation->run() === FALSE){
		$crud = new grocery_CRUD();
		$crud->set_table('nurses');
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$output = $crud->render();
		$data['nama'] = $this->session->userdata('nama');
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('lbulanan.php',$data);
		$this->load->view('layout/footer.php',(array)$output);
	}else{
		$this->M_laporan->lbulanan();
		redirect('perawat/index');
	}	

}

public function tahunan(){
	$tang=$this->M_laporan->ltahun();
	$l=FALSE;
	foreach($tang as $tgl) {
		$l=$tgl->tahun;
	}
	$datestring = "%Y";
	$tg=mdate($datestring);
	if($l===$tg){
		$crud = new grocery_CRUD();
		$crud->set_table('nurses');
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$output = $crud->render();
		$data['nama'] = $this->session->userdata('nama');
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('lata.php',$data);
		$this->load->view('layout/footer.php',(array)$output);
	}else{
	$crud = new grocery_CRUD();
	$crud->set_table('nurses');
	((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	$output = $crud->render();
	$data['nama'] = $this->session->userdata('nama');
	$this->load->view('layout/header.php',(array)$output);
	$this->load->view('ltahunan.php',$data);
	$this->load->view('layout/footer.php',(array)$output);
	}
}

public function input_tahunan(){
	$this->form_validation->set_rules('judul','judul', 'required');
	$this->form_validation->set_rules('isi','isi', 'required');
	if($this->form_validation->run() === FALSE){
		$crud = new grocery_CRUD();
		$crud->set_table('nurses');
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$output = $crud->render();
		$data['nama'] = $this->session->userdata('nama');
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('ltahunan.php',$data);
		$this->load->view('layout/footer.php',(array)$output);
	}else{
		$this->M_laporan->ltahunan();
		redirect('perawat/index');
	}	

}

}
