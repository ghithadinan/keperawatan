<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class LogBook extends CI_Controller
{
	protected $_view = 'logbook';

	function __construct()
	{
		parent::__construct();
        $this->load->database();
        $this->load->helper('url');
	}

	public function add()
	{
		$this->load->model('M_Nurses');

		$idRuangan = $this->session->userdata('id_ruangan');
		$data['nurse'] = $this->M_Nurses->getByRoom($idRuangan)->get()->result();

		$this->load->view('layout/header');
        $this->load->view($this->_view.'/index', $data);
        $this->load->view('layout/footer');
	}

	public function getDetail()
	{
		$this->load->model('M_Activity');
		$this->load->model('M_Nurses');
		$this->load->model('M_Jadwal');
		$this->load->model('M_Shift');
		$this->load->model('M_Logbook');

		$shift = NULL;
		$shiftId = NULL;

		$pkId    = $this->session->userdata('pk_id');
		$nurseId = $this->input->get('nurse_id');
		$date    = date('Y-m-d', strtotime($this->input->get('date')));

		$nurse    = $this->M_Nurses->getByIdWithRelation($nurseId, ['room', 'pk']);
		$schedule = $this->M_Jadwal->getByNurseRuanganTglJadwal($nurseId, $nurse->id_ruangan, $date);

		if($schedule) {
			$shift = $this->M_Shift->getById($schedule->id_shift)->row();
			if($shift)
				$shiftId = $shift->id_shift;
		}

		$listData = [];


		$shiftStartTime = !empty($shift->jam_masuk) ? substr($shift->jam_masuk, 0, 5) : '08:00';
		$shiftEndTime   = !empty($shift->jam_masuk) ? substr($shift->jam_pulang ,0, 5): '14:00';

		$defStartTime = strtotime($shiftStartTime);
		$defEndTime   = strtotime($shiftEndTime);
		$perDuration  = 60;
		$parseTime = round(abs($defEndTime - $defStartTime) / 60,2);
		$duration = $parseTime / $perDuration;

		$x = 0;
		$startTime = $shiftStartTime;
		while($x <= $duration) {
			$selectedTime = $startTime;

			$qCurrent = $this->M_Logbook->getData([
					'date' => $date,
					'time' => $selectedTime,
					'nurse_id' => $nurseId,
					'pk_id'    => $pkId,
					//'shift_id' => $shiftId
				])->row();

			$listData[$selectedTime] = [
				'id' => !empty($qCurrent) ? $qCurrent->id : '',
				'date'     => $date,
				'nurse_id' => $nurseId,
				'pk_id'    => $pkId,
				'shift_id' => $shiftId,
				'activity_id'  => !empty($qCurrent->activity_id) ? $qCurrent->activity_id : '',
				'patient_name' => !empty($qCurrent->patient_name) ? $qCurrent->patient_name : ''
			];

			$endTime = strtotime('+'.$perDuration.'minutes', strtotime($selectedTime));
			$time    = date('H:i', $endTime);
			$startTime = $time;
			$x++;
		}

		$data['nurse'] = $nurse;
		$data['info'] = [
			'date' => date('d/m/Y', strtotime($date)),
			'nurseName' => $nurse->nama,
			'roomName'  => $nurse->room ? $nurse->room->nama_ruangan : '-',
			'pkName'    => $nurse->room ? $nurse->pk->nama_pk : '-',
			'shiftName' => $shift ? $shift->nama_shift : '-'
		];

		$data['activity'] = $this->M_Activity->getByPk($pkId)->result();
		// $data['activity'] = $this->M_Activity->getData()->result();
		$data['listData'] = $listData;

		header('Content-Type: application/json');
		echo json_encode($data); die();
	}

	public function save()
	{
		$this->load->model('M_Logbook');

		$time = $this->input->post('time');

		if(count($time)) {
			foreach ($time as $idx => $value) {
				$id = $this->input->post('id')[$idx];
				$date        = $this->input->post('date')[$idx];
				$time        = $this->input->post('time')[$idx];
				$nurseId     = $this->input->post('nurse_id')[$idx];
				$activityId  = $this->input->post('activity_id')[$idx];
				$pkId        = $this->input->post('pk_id')[$idx];
				$shiftId     = $this->input->post('shift_id')[$idx];
				$patientName = $this->input->post('patient_name')[$idx];

				$input = [
					'date' => $date,
					'time' => $time,
					'nurse_id'     => $nurseId,
					'pk_id'        => $pkId,
					'shift_id'     => $shiftId,
					'activity_id'  => $activityId,
					'patient_name' => $patientName
				];

				$qCurrent = $this->M_Logbook->getData([
					'date' => $date,
					'time' => $time,
					'nurse_id' => $nurseId,
					'pk_id'    => $pkId,
					'shift_id' => $shiftId
				])->row();

				if($qCurrent) {
					$this->M_Logbook->updateData($qCurrent->id, $input);
				} else {
					$this->M_Logbook->createData($input);
				}
			}
		}

		header('Content-Type: application/json');
		echo json_encode(['message' => 'Data berhasil disimpan']); die();
	}

	public function report()
	{
		$this->load->model('M_Nurses');
		$this->load->model('M_Activity');
		$this->load->model('M_Logbook');
		$this->load->model('M_Ruangan');

		$roomId = $this->session->userdata('id_ruangan');
		$postitionName = $this->session->userdata('jabatan');

		$month   = $this->input->post('month');
		$year    = $this->input->post('year');
		$nurseId = $this->input->post('nurse_id');

		if($month && $year && $nurseId) {
			$dayCount = $this->getDaysInMonth($month, $year);

			$listData = [];

			$nurse = $this->M_Nurses->getByIdWithRelation($nurseId, ['room', 'pk']);

			$activities = $this->M_Activity->getByPk($nurse->id_pk)->result();

			foreach ($activities as $key => $dt) {

				$days = [];

				for ($i=1; $i <= $dayCount; $i++) {

					$selectedDate = $year.'-'.str_pad($month, 2, '0', STR_PAD_LEFT).'-'.str_pad($i, 2, '0', STR_PAD_LEFT);
					$qCheck = $this->M_Logbook->getData(['nurse_id' => $nurseId, 'activity_id' => $dt->id, 'date' => $selectedDate]);

					$value = $qCheck->num_rows();

					$days[$i] = [
						'activity_id' => $dt->id,
						'day'   => $i,
						'month' => $month,
						'year'  => $year,
						'value' => $value
					];
				}

				$listData[] = [
					'activity_id' => $dt->id,
					'activity_name' => $dt->name,
					'days' => $days
				];
			}

			$data['info'] = [
				'month' => $month,
				'year'  => $year,
				'nurseName' => $nurse->nama,
				'roomName'  => $nurse->room ? $nurse->room->nama_ruangan : '-',
				'pkName'    => $nurse->room ? $nurse->pk->nama_pk : '-',
			];
			$data['dayCount'] = $dayCount;
			$data['listData'] = $listData;
		}

		$nurses = [];
		if($postitionName != 'Kepala Bidang') {
			$nurses = $this->M_Nurses->getByRoom($roomId)->get()->result();
		}

		$data['nurses']   = $nurses;
		$data['rooms']    = $this->M_Ruangan->getData()->result();
		$data['optMonth'] = $this->getListMonth();
		$data['optYear']  = $this->getListYear(2015);

		$this->load->view('layout/header');
        $this->load->view($this->_view.'/report', $data);
        $this->load->view('layout/footer');
	}

	public function reportDetail()
	{
		$this->load->model('M_Activity');
		$this->load->model('M_Logbook');

		$day        = $this->input->post('day');
		$month      = $this->input->post('month');
		$year       = $this->input->post('year');
		$activityId = $this->input->post('activity_id');
		$nurseId    = $this->input->post('nurse_id');

		$selectedDate = $year.'-'.str_pad($month, 2, '0', STR_PAD_LEFT).'-'.str_pad($day, 2, '0', STR_PAD_LEFT);
		$listData = $this->M_Logbook->getData(['nurse_id' => $nurseId, 'activity_id' => $activityId, 'date' => $selectedDate])->result();

		foreach ($listData as $key => &$dt) {
			$activity = NULL;
			if($dt->activity_id) {
				$activity = $this->M_Activity->getById($dt->activity_id);
			}

			$dt->activity = $activity;
		}

		$data['date'] = str_pad($day, 2, '0', STR_PAD_LEFT).'/'.str_pad($month, 2, '0', STR_PAD_LEFT).'/'.$year;
		$data['data'] = $listData;

		$this->load->view($this->_view.'/report-detail', $data);
	}

	public function getListNurseByRoom($roomId)
	{
		$this->load->model('M_Nurses');
		$data = $this->M_Nurses->getByRoom($roomId)->get()->result();
		header('Content-Type: application/json');
		echo json_encode(['data' => $data]); die();
	}	

	private function getDaysInMonth($month, $year)
	{
        return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
    }

    private function getListMonth($type = '', $default = null)
    {
        $list = [];
        if (is_array($default)) {
            if (count($default)) {
                $list = $default;
            } else {
                $list = ['' => '-'];
            }
        }

        if ($type == 'm') {
            $data = [
                '01' => 'Januari',
                '02' => 'February',
                '03' => 'Maret',
                '04' => 'April',
                '05' => 'Mei',
                '06' => 'Juni',
                '07' => 'Juli',
                '08' => 'Agustus',
                '09' => 'September',
                '10' => 'Oktober',
                '11' => 'November',
                '12' => 'Desember'
            ];
        } else {
            $data = [
                '1' => 'Januari',
                '2' => 'February',
                '3' => 'Maret',
                '4' => 'April',
                '5' => 'Mei',
                '6' => 'Juni',
                '7' => 'Juli',
                '8' => 'Agustus',
                '9' => 'September',
                '10' => 'Oktober',
                '11' => 'November',
                '12' => 'Desember'
            ];
        }

        foreach ($data as $key => $value) {
            $list[$key] = $value;
        }

        return $list;
    }

    private function getListYear($min = 2000, $max = NULL, $order = 'desc', $default = '')
    {

        if ($max == NULL)
            $max = date('Y');
        $data = [];
        if ($default) {
            $data[''] = $default;
        }

        if ($order == 'desc') {
            for ($i = $max; $i >= $min; $i--) {
                $data[$i] = $i;
            }
        } else {
            for ($i = $min; $i <= $max; $i++) {
                $data[$i] = $i;
            }
        }

        return $data;
    }
}
