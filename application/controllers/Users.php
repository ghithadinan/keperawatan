<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
    }

    public function index() {
        $crud = new grocery_CRUD();
        $crud->set_table('nurses');
        ((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
        $output = $crud->render();

        $this->load->view('layout/header.php', (array) $output);
        $this->load->view('login.php');
        $this->load->view('layout/footer.php', (array) $output);
    }

    public function login() {
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Pasword', 'required');

        if ($this->form_validation->run() === FALSE) {
            $crud = new grocery_CRUD();
            $crud->set_table('nurses');
            ((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
            $output = $crud->render();

            $this->load->view('layout/header.php', (array) $output);
            $this->load->view('login.php');
            $this->load->view('layout/footer.php', (array) $output);
        } else {

            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $user_id = $this->user_model->login($username, $password);

            if ($user_id) {
                foreach ($user_id as $row) {
                    $id = $row->id_nurse;
                    $nama = $row->nama;
                    $jabatan = $row->nama_jabatan;
                    $id_jabatan = $row->id_jabatan;
                    $nira = $row->nira;
                    $id_r = $row->id_ruangan;
                    $pkId = $row->id_pk;
                    $ruangan = $row->nama_ruangan;
                    $imge = $row->image;
                }

                $user_data = array(
                    //'user_id' => $user_id,
                    'user_id' => $id,
                    'nama' => $nama,
                    'jabatan' => $jabatan,
                    'id_jabatan' => $id_jabatan,
                    'nira' => $nira,
                    'id_ruangan' => $id_r,
                    'pk_id' => $pkId,
                    'ruangan' => $ruangan,
                    'img' => $imge,
                    'logged_in' => true
                );


                $this->session->set_userdata($user_data);
                $this->session->set_flashdata('user_loggedin', 'Anda berhasih login');
                redirect('perawat/about');
            } else {
                $this->session->set_flashdata('login_failed', 'Silahkan hubungi Admin');
                redirect('users/index');
            }
        }
    }

    public function logout() {
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('nama');
        $this->session->unset_userdata('jabatan');
        $this->session->unset_userdata('id_jabatan');
        $this->session->unset_userdata('nira');
        $this->session->unset_userdata('id_ruangan');
        $this->session->unset_userdata('pk_id');
        $this->session->unset_userdata('ruangan');
        $this->session->unset_userdata('img');
        //$this->session->sess_destroy();
        redirect('users/index');
    }

}
