<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}
	public function index()
	{
		$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('nurses');
			$output = $crud->render();
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$data['x']=$this->M_Profile->perawat_ruangan();
		$data['y']=$this->M_Profile->golongan_ruang();
		$data['z']=$this->M_Profile->pk_ruang();
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('perawat.php', $data);
		$this->load->view('layout/footer.php');
	}

	public function perawat($id)
	{

		//$id=$this->input->post('id');
        $data=$this->M_Profile->get_perawat($id);
        echo json_encode($data);
	}


}
