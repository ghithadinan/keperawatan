<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perawat extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}
	public function index()
	{
		$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('nurses');
			$output = $crud->render();
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$data['judul']='HOME';
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('login.php', $data);
		$this->load->view('layout/footer.php');
	}

	public function show_perawat()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('nurses');
			$crud->set_relation('id_gol','golongan','nama_gol');
			$crud->display_as('id_gol','Gol');
			$crud->set_relation('id_pk','tugas_pokok','nama_pk');
			$crud->display_as('id_pk','PK');
			$crud->set_relation('id_jabatan','jabatan','nama_jabatan');
			$crud->display_as('id_jabatan','Jabatan');
			$crud->set_relation('jk','jenis_kelamin','nama_jk');
			$crud->display_as('jk','JK');
			$crud->display_as('id_jurusan','Jurusan');
			$crud->set_relation('id_ruangan','ruangan','nama_ruangan');
			$crud->display_as('id_ruangan','Ruangan');
			$crud->set_relation('id_status_kepegawaian','status_kepegawaian','status_kepegawaian');
			$crud->display_as('id_status_kepegawaian','Status Pegawai');
			$crud->set_relation('id_pendidikan','pendidikan','nama_pendidikan');
			$crud->display_as('id_pendidikan','Pendidikan');
			$crud->display_as('id_pelatihan','Pelatihan');
			$crud->display_as('image','Img');
			$crud->display_as('nama','Nama Lengkap');
			$crud->set_subject('Perawat');
			$crud->set_language('indonesian');
			$crud->unset_clone();
			$crud->unset_texteditor('id_pelatihan','full_text');
			$crud->required_fields('nama');
			//$crud->field_type('id_pk', 'interger');
			$crud->set_field_upload('image','assets/uploads/files');

			$output = $crud->render();
			//$this->load->view('example.php',(array)$output);
			
			$this->read_show_perawat($output);
	}

	public function read_show_perawat($output = null)
	{
		if(!$this->session->userdata('logged_in')){
			redirect('users/index');
		}
		$data['judul']='PERAWAT';
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('example.php', $data);
		$this->load->view('layout/footer.php');
	}

	public function about(){
		if(!$this->session->userdata('logged_in')){
			redirect('users/index');
		}
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('nurses');
			$output = $crud->render();
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));


		$id=$this->session->userdata('user_id');
		$data['records']=$this->M_Profile->get_data_individu($id);
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('content/about.php', $data);
		$this->load->view('layout/footer.php');

	}
}