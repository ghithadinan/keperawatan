<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arencana extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function bulanan(){
		if(!$this->session->userdata('logged_in')){
			redirect('users/index');
		}
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('nurses');
			$output = $crud->render();
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));

		//$data['judul']='Rencana Kegiatan Bulanan';
		$data['y']=$this->M_Arencana->ruangan();
		$data['x']=$this->M_Arencana->bulanans();
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('arbulanan.php', $data);
		$this->load->view('layout/footer.php');

	}

	public function tahunan(){
		if(!$this->session->userdata('logged_in')){
			redirect('users/index');
		}
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('nurses');
			$output = $crud->render();
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$data['judul']='Rencana Kegiatan Bulanan';
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('artahunan.php', $data);
		$this->load->view('layout/footer.php');

	}

	public function bul()
	{
		$th=$this->input->post('th');
		$bl=$this->input->post('bl');
		$id=$this->input->post('id');
		$data=$this->M_Arencana->tampil_bul($th,$bl,$id);
		echo json_encode($data);
		//$this->load->view('layout/header');
		//$this->load->view('v_perawat');
		//$this->load->view('layout/footer');
	}

}