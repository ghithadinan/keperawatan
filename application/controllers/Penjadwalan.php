<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Penjadwalan extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
    }

    public function add() {
        $crud = new grocery_CRUD();
        $crud->set_table('nurses');
        ((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
        $output = $crud->render();
        $keluaran['k'] = $this->M_Jadwal->tampil();

        $this->load->view('layout/header', (array) $output);
        $this->load->view('jadwal', $keluaran);
        $this->load->view('layout/footer');
    }

    public function tampilkan() {
        $this->load->model('M_Shift');
        $this->load->model('M_Jadwal');

        $tahun = $this->input->post('t');
        $bulan = $this->input->post('b');
        $shift = $this->M_Shift->getAll();

        $idRuangan = $this->session->userdata('id_ruangan');
        $nurseId = $this->input->post('nurseId');
        $jadwal = $this->M_Jadwal->getListByRuanganNurseId($idRuangan, $nurseId);

        $nurseJadwal = [];
        if (!empty($jadwal)) {
            foreach ($jadwal as $jd) {
                $nurseJadwal[$jd->tgl_jadwal] = (int) $jd->id_shift;
            }
        }

        echo json_encode(array(
            'days' => $this->get_days_in_month($bulan, $tahun),
            'shift' => $shift,
            'jadwal' => $nurseJadwal
        ));
    }

    public function update() {
        $this->load->model('M_Nurses');
        $this->load->model('M_Shift');
        $this->load->model('M_Jadwal');

        $post = $this->input->post();

        $success = FALSE;
        $message = 'Data Suster tidak di temukan';

        $nurse = $this->M_Nurses->getById($post['nurseId']);
        if (!empty($nurse)) {
            $message = 'Data Shift tidak di temukan';
            $shift = $this->M_Shift->getById($post['idShift']);
            if ($post['idShift'] == 0) {
                $shift = 'nullShift';
            }
            if (!empty($shift)) {
                $message = 'Gagal mengubah data Jadwal';

                $idRuangan = $this->session->userdata('id_ruangan');
                $jadwal = $this->M_Jadwal->getByNurseRuanganTglJadwal($nurse->id_nurse, $idRuangan, $post['tglJadwal']);

                $data = array(
                    'id_nurse' => $nurse->id_nurse,
                    'id_ruangan' => $idRuangan,
                    'id_shift' => $post['idShift'],
                    'tgl_jadwal' => $post['tglJadwal'],
                );

                if (empty($jadwal)) { //create
                    if ($this->M_Jadwal->create($data)) {
                        $success = TRUE;
                        $message = 'Berhasil mengubah data Jadwal';
                    }
                } else {
                    if ($post['idShift'] != 0) { // update
                        $where = array(
                            'id_nurse' => $nurse->id_nurse,
                            'id_ruangan' => $idRuangan,
                            'tgl_jadwal' => $post['tglJadwal'],
                        );
                        $update = array(
                            'id_shift' => $post['idShift'],
                        );
                        if ($this->M_Jadwal->update($where, $update)) {
                            $success = TRUE;
                            $message = 'Berhasil mengubah data Jadwal';
                        }
                    } else { // delete
                        $delete = array(
                            'id_nurse' => $nurse->id_nurse,
                            'id_ruangan' => $idRuangan,
                            'tgl_jadwal' => $post['tglJadwal'],
                        );
                        if ($this->M_Jadwal->delete($delete)) {
                            $success = TRUE;
                            $message = 'Berhasil mengubah data Jadwal';
                        }
                    }
                }
            }
        }

        echo json_encode(array(
            'success' => $success,
            'message' => $message
        ));
    }

    private function get_days_in_month($month, $year) {
        return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
    }

    public function laporan() {
        $crud = new grocery_CRUD();
        $crud->set_table('nurses');
        ((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
        $output = $crud->render();
        $keluaran['k'] = $this->M_Jadwal->tampilJadwal();
        $this->load->model('M_Shift');
        $keluaran['s'] = $this->M_Shift->getAll();
        $keluaran['session'] = $this->session->userdata();

        $this->load->view('layout/header', (array) $output);
        $this->load->view('lapjadwal', $keluaran);
        $this->load->view('layout/footer');
    }

    public function tampilkans() {
        $this->load->model('M_Shift');
        $this->load->model('M_Jadwal');

        $tahun = $this->input->post('t');
        $bulan = $this->input->post('b');
        $shift = $this->M_Shift->getAll();

        $idRuangan = $this->input->post('r');
        $nurseId = $this->input->post('nurseId');
        $jadwal = $this->M_Jadwal->getListByRuanganNurseId($idRuangan, $nurseId);

        $nurseJadwal = [];
        if (!empty($jadwal)) {
            foreach ($jadwal as $jd) {
                $nurseJadwal[$jd->tgl_jadwal] = (int) $jd->id_shift;
            }
        }

        echo json_encode(array(
            'days' => $this->get_days_in_month($bulan, $tahun),
            'shift' => $shift,
            'jadwal' => $nurseJadwal
        ));
    }

    public function lapJadwal() {
        $post = $this->input->post();
        echo json_encode($this->getDataJadwal($post['t'], $post['b'], $this->get_days_in_month($post['b'], $post['t']), $post['r'], $post['s'], $post['n']));
    }

    private function getDataJadwal($tahun, $bulan, $days, $ruangan, $shift, $idNurse) {
        $this->load->model('M_Nurses');

        if (strlen((String) $bulan) == 1) {
            $bulan = 0 . $bulan;
        }

        $tglJadwalArr = [];
        for ($i = 1; $i <= $days; $i++) {
            $daysV = $i;
            if (strlen((String) $i) == 1) {
                $daysV = 0 . $i;
            }
            $tglJadwal = $tahun . '-' . $bulan . '-' . $daysV;
            $tglJadwalArr[] = $tglJadwal;
        }

        $whereNurses = [
            'id_ruangan' => $ruangan,
            'id_nurse' => $idNurse
        ];

        if ($ruangan == 0) {
            unset($whereNurses['id_ruangan']);
        }

        if ($idNurse == 0) {
            unset($whereNurses['id_nurse']);
        }

        $nursesData = [];
        $nursesJadwal = [];

        $nurses = $this->M_Nurses->getListBy($whereNurses);
        if (!empty($nurses)) {
            foreach ($nurses as $nurse) {
                $jadwals = [];
                for ($i = 1; $i <= $days; $i++) {
                    $daysV = $i;
                    if (strlen((String) $i) == 1) {
                        $daysV = 0 . $i;
                    }
                    $tglJadwal = $tahun . '-' . $bulan . '-' . $daysV;

                    $jadwalCond = [
                        'jadwal.id_nurse' => $nurse->id_nurse,
                        'jadwal.id_ruangan' => $ruangan,
                        'jadwal.id_shift' => $shift,
                        'jadwal.tgl_jadwal' => $tglJadwal
                    ];

                    if ($ruangan == 0) {
                        unset($jadwalCond['jadwal.id_ruangan']);
                    }
                    if ($shift == 0) {
                        unset($jadwalCond['jadwal.id_shift']);
                    }

                    $getJadwal = $this->M_Jadwal->getShiftBy($jadwalCond);
                    $jadwalsKey = $nurse->id_nurse . '-' . $tglJadwal;
                    $jadwals[$jadwalsKey] = $getJadwal;
                }

                $nursesJadwal[$nurse->id_nurse] = $jadwals;
                $nursesData[] = $nurse;
            }
        }

        return array(
            'days' => $days,
            'nursesJadwal' => $nursesJadwal,
            'nurses' => $nursesData
        );
    }

    public function getNurses() {
        $this->load->model('M_Nurses');

        $idRuangan = $this->input->post('id');
        if ($idRuangan != 0) {
            $nurses = $this->M_Nurses->getListBy([
                'id_ruangan' => $idRuangan
            ]);
        } else {
            $nurses = $this->M_Nurses->getAll();
        }

        $options = array();
        $options[] = array('value' => '0', 'label' => 'All');
        foreach ($nurses as $nurse) {
            $options[] = array('value' => $nurse->id_nurse, 'label' => $nurse->nama);
        }

        echo json_encode($options);
    }

    public function lapJadwalPdf() {
        $this->load->model('M_Shift');
        $this->load->model('M_Ruangan');
        $this->load->model('M_Nurses');

        $data['title'] = 'JADWAL DINAS';
        $post = $this->input->get();
        $data['jadwal'] = $this->getDataJadwal($post['t'], $post['b'], $this->get_days_in_month($post['b'], $post['t']), $post['r'], $post['s'], $post['n']);
        $data['post'] = $post;
        $data['bulan'] = $this->get_list_month($post['b']);
        $data['shift'] = $this->M_Shift->getById($post['s']);
        $data['ruangan'] = $this->M_Ruangan->getById($post['r']);
        $data['nurse'] = $this->M_Nurses->getById($post['n']);

//        $this->load->view('laporan/penjadwalan', $data);

        $html = $this->load->view('laporan/penjadwalan', $data, true);
        // pdf
        $this->load->library('M_pdf');
        $mpdf = $this->m_pdf->load([
            'mode' => 'utf-8',
            'format' => 'A4-L'
        ]);
        //$pdf->AddPage('c', 'A4-L');
        //$pdf->SetFooter([$this->input->server('HT  TP_HOST') . '|{PAGENO}|' . date(DATE_RFC822)]);
        $mpdf->WriteHTML(str_replace("'", '"', $html));
        $mpdf->Output('penjadwalan' . date('Ymdhis') . '.pdf', \Mpdf\Output\Destination::INLINE);
    }

    private function get_list_month($i) {
        $month = array(
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember'
        );
        return $month[$i];
    }

}
