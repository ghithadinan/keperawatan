<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rencana extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function bulanan(){
		if(!$this->session->userdata('logged_in')){
			redirect('users/index');
		}
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('nurses');
			$output = $crud->render();
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$data['judul']='Rencana Kegiatan Bulanan';
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('rbulanan.php', $data);
		$this->load->view('layout/footer.php');

	}

	public function input_rbulanan(){
		if(!$this->session->userdata('logged_in')){
			redirect('users/index');
		}
		$tang=$this->M_Rencana->input_bulanan();

		$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('nurses');
			$output = $crud->render();
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$data['judul']='Rencana Kegiatan Bulanan';
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('rbulanan.php', $data);
		$this->load->view('layout/footer.php');
	}

	public function tahunan(){
		if(!$this->session->userdata('logged_in')){
			redirect('users/index');
		}
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('nurses');
			$output = $crud->render();
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$data['judul']='Rencana Kegiatan Tahunan';
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('rtahunan.php', $data);
		$this->load->view('layout/footer.php');

	}

	public function input_rtahunan(){
		if(!$this->session->userdata('logged_in')){
			redirect('users/index');
		}
		$tang=$this->M_Rencana->input_tahunan();

		$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('nurses');
			$output = $crud->render();
		((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$data['judul']='Rencana Kegiatan Tahunan';
		$this->load->view('layout/header.php',(array)$output);
		$this->load->view('rtahunan.php', $data);
		$this->load->view('layout/footer.php');
	}
}