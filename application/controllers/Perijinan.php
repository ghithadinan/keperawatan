<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perijinan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
	}

	public function read_simk()
	{
		$data['posts']=$this->custom_query_model->get_list();
				if(!$this->session->userdata('logged_in')){
			redirect('users/index');
		}

		$this->load->view('simk.php',$data);

	}

	public function edit_simk()
	{
		$nira=$this->input->post('nira');
	    $id=$this->input->post('id');
		$this->custom_query_model->update_nira($id,$nira);
				/*if(!$this->session->userdata('logged_in')){
			redirect('users/index');
		}*/

		$this->load->view('simk.php');
	}

}
