<header id="header">
          <div class="container-fluid">
            <div class="row">
                <div class="col-10">
                  <h4><i class="material-icons" style="font-size:24px;">apps</i>
                  Laporan Harian</h4>
                </div>
                <div class="col-2"> </div>
            </div>
          </div>
        </header>

<div class="container" style="margin-top: 20px; padding: 10px;">
<p>Ruang:  <?php echo $this->session->userdata('ruangan'); ?></p>
<p>Silahkan Isi Laporan Harian anda: <?php echo $this->session->userdata('nama'); ?></p>

<hr>
	<?php echo validation_errors(); ?>
	<?php echo form_open('laporan/input_harian'); ?>
		<div class="form-group">
			    <label for="judul">Judul</label>
			    <input type="text" class="form-control" name="judul" placeholder="Judul Laporan">
			  </div>
			  <div class="form-group">
			    <label for="isi">Isi</label>
			    <textarea class="form-control" name="isi" id="editor1"></textarea>
			  </div>
			  <button type="submit" name="lharian" class="btn btn-success">Simpan</button>
	<?php echo form_close(); ?>
</div>
<script type="text/javascript">
	CKEDITOR.replace('editor1');
</script>
		