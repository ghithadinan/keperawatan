<div class="container">
    <div class="card">
        <div class="card-header bg-info text-white" style="padding: 4px 1.25rem; ">
            Penjadwalan
        </div>
        <div class="card-body">
            <div class="row" style="padding:20px;">
                <div class="col-sm">
                    <h7>Tahun :</h7>
                    <select class="form-control form-control-sm" name="tahun" id="tahun">
                        <option value="<?php echo date("Y"); ?>"><?php echo date("Y"); ?></option>
                        <option value="<?php echo date("Y", strtotime("+1 years")); ?>"><?php echo date("Y", strtotime("+1 years")); ?></option>
                    </select>
                </div>
                <div class="col-sm">
                    <h7>Bulan :</h7>
                    <select class="form-control form-control-sm" name="bulan" id="bulan">
                        <option value="">Bulan</option>
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                </div>
                <div class="col-sm">
                    <h7>Nama :</h7>
                    <select class="form-control form-control-sm" name="nurse" id="nurse">
                        <option value="">Nama</option>
                        <?php foreach ($k as $m): ?>
                            <option value="<?php echo $m->id_nurse; ?>"><?php echo $m->nama; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-sm form-control-sm" style="margin-top: 15px;">
                    <button class="btn btn-primary" id="tampil">Tampil</button>
                </div>
            </div>
            <hr>

            <div class= "row" id="jadwal" style="display: none;">
                <table class="table" id="table-jadwal">
                    <thead>
                        <tr>
                            <td>Tanggal</td>
                            <td>Hari</td>
                            <td>Shift</td>
                            <td>Jam Masuk</td>
                            <td>Jam Pulang</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var shift = [];
    $('#tampil').click(function () {
        $('#jadwal').hide();
        $('#table-jadwal tbody').html('');

        var t = $('#tahun').val();
        var b = $('#bulan').val();
        var nurseId = parseInt($('#nurse').val());

        if (t === '' || b === '' || isNaN(nurseId)) {
            alert('input tidak lengkap');
            return false;
        }

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "penjadwalan/tampilkan/",
            data: {
                t: t,
                b: b,
                nurseId: nurseId
            },
            success: function (result) {
                var data = JSON.parse(result);
                shift = data.shift;
                var days = parseInt(data.days);
                var jadwal = data.jadwal;
                var weekday = new Array(7);
                    weekday[0] = "Minggu";
                    weekday[1] = "Senin";
                    weekday[2] = "Selasa";
                    weekday[3] = "Rabu";
                    weekday[4] = "Kamis";
                    weekday[5] = "Jumat";
                    weekday[6] = "Sabtu";

                for (var ti = 1; ti <= days; ti++) {
                    var tiDate = ti.toString();
                    var tiB = b.toString();

                    if (tiDate.length === 1) {
                        tiDate = 0 + tiDate;
                    }
                    if (tiB.length === 1) {
                        tiB = 0 + tiB;
                    }
                    
                    var chari= t + '-' + tiB + '-' + tiDate;
                    var hari= chari.toString();
                    var ke = new Date(hari);
                    var ktmu = weekday[ke.getDay()];
                    var tbTi = t + '-' + tiB + '-' + tiDate;
                    var apn = '<tr id="tr-' + ti + '">';
                    apn += '<td>' + tbTi + '</td>';
                    apn += '<td>' + ktmu + '</td>';
                    apn += '<td><select name="shift" onchange="setShift(this)" style="width:100px;" data-nurse-id="' + nurseId + '" data-tgl-jadwal="' + tbTi + '">';
                    apn += '<option value="0">Pilih Shift</option>';

                    var shiftId = jadwal[tbTi];
                    var jamMasuk = '';
                    var jamKeluar = '';

                    if (shiftId === undefined) {
                        $.each(data.shift, function (i, v) {
                            apn += '<option value="' + v.id_shift + '">' + v.nama_shift + '</option>';
                        });
                    } else {
                        $.each(data.shift, function (i, v) {
                            var selected = '';
                            if (parseInt(shiftId) === parseInt(v.id_shift)) {
                                selected = 'selected';
                            }
                            apn += '<option value="' + v.id_shift + '" ' + selected + '>' + v.nama_shift + '</option>';
                        });

                        var getShift = null;
                        $.each(shift, function (i, v) {
                            if (parseInt(v.id_shift) === parseInt(shiftId)) {
                                getShift = v;
                            }
                        });

                        if (getShift !== null) {
                            if (getShift.jam_masuk !== null) {
                                jamMasuk = getShift.jam_masuk;
                            }
                            if (getShift.jam_pulang !== null) {
                                jamKeluar = getShift.jam_pulang;
                            }
                        }
                    }

                    apn += '</select></td>';
                    apn += '<td class="td-jam-masuk">' + jamMasuk + '</td>';
                    apn += '<td class="td-jam-keluar">' + jamKeluar + '</td>';
                    apn += '<tr>';

                    $('#table-jadwal tbody').append(apn);
                }
                $('#jadwal').show();
            },
            error: function (data) {
                alert(data.statusText);
            }
        });
    });

    function setShift(index) {
        $.ajax({
            url: "<?php echo base_url(); ?>" + "penjadwalan/update/",
            type: 'POST',
            dataType: 'json',
            data: {
                nurseId: parseInt($(index).data('nurse-id')),
                idShift: parseInt($(index).val()),
                tglJadwal: $(index).data('tgl-jadwal')
            },
            cache: false,
            success: function (data) {
                if (data.success) {
                    var value = $(index).val();
                    var getShift = null;

                    $.each(shift, function (i, v) {
                        if (parseInt(v.id_shift) === parseInt(value)) {
                            getShift = v;
                        }
                    });

                    var trId = $(index).parent().parent().attr('id');
                    $('#table-jadwal tbody tr').each(function () {
                        if ($(this).attr('id') === trId) {
                            var tdJamMasuk = $(this).find('.td-jam-masuk');
                            var tdJamKeluar = $(this).find('.td-jam-keluar');
                            tdJamMasuk.html('');
                            tdJamKeluar.html('');
                            if (getShift !== null) {
                                if (getShift.jam_masuk !== null) {
                                    tdJamMasuk.html(getShift.jam_masuk);
                                }
                                if (getShift.jam_pulang !== null) {
                                    tdJamKeluar.html(getShift.jam_pulang);
                                }
                            }
                        }
                    });
                } else {
                    alert(data.message);
                }
            },
            error: function (data) {
                alert(data.statusText);
            }
        });
    }
</script>
