<!DOCTYPE html>
  <html>
    <head>
      <title>E-Nurse | RSUD Ciamis</title>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
      <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"  />
      <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css"  />  
      
   <style type="text/css">
     #header{
      margin-bottom: 10px;
     }
     #ket{
      margin-bottom: 5px;
     }
     .editbox{
      display: none;
     }
   </style>  
    </head>

    <body>

          <div class="container-fluid" id="h" style="background-color: white;"><div class="row" id="rr" style="height: 70px;"> <div class="col-1" id="rr"><img src="<?php echo base_url();?>assets/img/rsu-logo.png" style="width: 60px; padding: 5px;"></div><div class="col" id="rr"><h3 style="margin-top:20px; font-family: Times New Roman; color: #7a378b;">Sistem Informasi e-Nursing RSUD Ciamis</h3></div></div></div>
          <nav class="navbar navbar-expand-md navbar-dark" style="background-color: #7a378b;">
           <!--  <a class="navbar-brand" href="#">E-Nursing | RSUD Ciamis</a> -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <?php if(!$this->session->userdata('logged_in')): ?>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>users/index">Login</a>
              </li>
            <?php endif; ?>
            <?php if($this->session->userdata('logged_in') AND $this->session->userdata('jabatan')=='Kepala Bidang'): ?>
               <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Input Data
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('perawat/show_perawat/add')?>">Data Perawat</a>
                <div class="dropdown-divider"></div>
                  <!-- <a class="dropdown-item" href="<?php echo site_url('logbook/add')?>">Data Logbook</a> -->
              </li>
               <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Laporan
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('perawat/show_perawat')?>">Data Perawat</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('penjadwalan/laporan')?>">Data Jadwal</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('logbook/report')?>">Data Logbook</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Master
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('master/golongan')?>">Golongan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('master/jabatan')?>">Jabatan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('master/ruangan')?>">Ruangan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('master/pendidikan')?>">Pendidikan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('master/activity')?>">Kegiatan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('master/activityPk')?>">Kegiatan PK</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Perijinan
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('perijinan/read_simk')?>">SIM-K</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('perijinan/str')?>">STR</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('perijinan/sipp')?>">SIPP</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Print
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('cetaks/index')?>">Status Kepegawaian</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('cetaks/laporan')?>">Golongan Ruang</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('logbook/report')?>">Data Logbook</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>charts/index">Charts</a>
              </li>
              <?php endif; ?>
               <?php if($this->session->userdata('logged_in') AND $this->session->userdata('jabatan')=='Kepala Ruangan'): ?>
               <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Laporan
                </a>
                 <div class="dropdown-menu" aria-labelledby="navbarDropdown"><!--
                  <a class="dropdown-item" href="<?php // echo site_url('laporan/harian')?>">Harian</a>--><?php //endif; ?> 
                 
                  <!-- <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php //echo site_url('laporan/bulanan')?>">Bulanan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php //echo site_url('laporan/tahunan')?>">Tahunan</a> -->
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('logbook/report')?>">Logbook</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('penjadwalan/laporan')?>">Jadwal</a>
                  </div></li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Input Data
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('penjadwalan/add')?>">Data Jadwal</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('logbook/add')?>">Data Logbook</a>
                </div>
              </li>
                 <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Rencana Kegiatan
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php // echo site_url('rencana/bulanan')?>">Bulanan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php //echo site_url('rencana/tahunan')?>">Tahunan</a>
                </div> -->

                <?php endif; ?>
              
              <?php if($this->session->userdata('logged_in')): ?>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

        <img src="<?php echo base_url();?>assets/uploads/files/<?php echo $this->session->userdata("img");?>" alt="Card image cap" style="width: 30px; height: 30px; border: 1px solid #ddd;">
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo base_url(); ?>perawat/about">Profile</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo base_url(); ?>users/logout">Logout</a>
                </div>
              </li>
              <?php endif; ?>
            </ul>
            <!--<form class="form-inline my-2 my-lg-0">-->
              <!--<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>-->
            <!--</form>-->
          </div>
        </nav>



        <?php if($this->session->flashdata('login_failed')):?>
        <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>';?>
        <?php endif; ?>

<div style="margin-top: 15px;"></div>
<div class="container table-responsive">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">NIRA</th>
      <th scope="col">NIP</th>
      <th scope="col">NAMA</th>
      <th scope="col">RUANGAN</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($posts as $post) :?>
  <?php $id=$post->id_simk; ?>
    <tr id="<?php echo $id ?>" class="edit_tr">
    
    <?php $post->id_nurse; ?>
      <td class="edit_td"><span id='one_<?php echo $id; ?>' class='text'><?php echo $post->nira; ?></span><input type='text' name='nira' value='<?php echo $post->nira; ?>' class='editbox' id='one_input_<?php echo $id;?>'/></td>
      <td><?php echo $post->nip; ?></td>
      <td><?php echo $post->nama; ?></td>
      <td><?php echo $post->nama_ruangan; ?></td>

    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
</div>


<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>



        <script type="text/javascript">
        $(document).ready(function()
        {
        $(".edit_tr").click(function()
        {
        var ID=$(this).attr('id');
        $("#one_"+ID).hide();
        $("#one_input_"+ID).show();
        }).change(function()
        {
        var ID=$(this).attr('id');
        var nira=$("#one_input_"+ID).val();
        var dataString = 'id='+ID+'&nira='+nira;
        
        $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>"+"perijinan/edit_simk/",
        data:{nira:nira,id:ID},
        success: function(html)
        {
        $("#one_"+ID).html(nira);
        }
        });
      });
       
        
        
        $(".editbox").mouseup(function() 
        {
        return false
        });
        
        
        $(document).mouseup(function()
        {
          $(".editbox").hide();
          $(".text").show();
        });
        
      });
        </script>
    </body>
  </html>