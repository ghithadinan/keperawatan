<style>
    .middle-table {
        vertical-align: middle !important;
    }
</style>

<div class="container">
    <div class="card">
        <div class="card-header bg-info text-white" style="padding: 4px 1.25rem; ">
            Jadwal
        </div>
        <div class="card-body">
            <div class="row" style="padding:20px;">
                <div class="col-sm">
                    <h7>Tahun :</h7>
                    <select class="form-control form-control-sm" name="tahun" id="tahun">
                        <option value="<?php echo date("Y"); ?>"><?php echo date("Y"); ?></option>
                        <option value="<?php echo date("Y", strtotime("+1 years")); ?>"><?php echo date("Y", strtotime("+1 years")); ?></option>
                    </select>
                </div>
                <div class="col-sm">
                    <h7>Bulan :</h7>
                    <select class="form-control form-control-sm" name="bulan" id="bulan">
                        <option value="">Bulan</option>
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                </div>
                <div class="col-sm">
                    <h7>Shift :</h7>
                    <select class="form-control form-control-sm" name="shift" id="shift">
                        <option value="0">All</option>
                        <?php foreach ($s as $sd): ?>
                            <option value="<?php echo $sd->id_shift; ?>"><?php echo $sd->nama_shift; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-sm">
                    <h7>Ruangan :</h7>
                    <select class="form-control form-control-sm" name="ruangan" id="ruangan">
                        <option value="0">All</option>
                        <?php foreach ($k as $m): ?>
                            <option value="<?php echo $m->id_ruangan; ?>"><?php echo $m->nama_ruangan; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-sm">
                    <h7>Nama :</h7>
                    <select class="form-control form-control-sm" id="nurses">
                        <option value="0">All</option>
                    </select>
                </div>
                <div class="col-sm form-control-sm" style="margin-top: 15px;">
                    <button class="btn btn-primary" id="tampil">Tampil</button>
                </div>
            </div>
            <hr>
            <div id="jadwal" class="table-responsive-md">
                <table class="table table-bordered" id="table-jadwal">
                    <tr valign="middle">
                        <th id="td-no" class="middle-table">No</th>
                        <th id="td-nama" class="middle-table">Nama</th>
                        <th id="td-jabatan" class="middle-table">Jabatan</th>
                        <th id="td-pendidikan" class="middle-table">Pendidikan</th>
                        <th class="text-center middle-table" id="td-tanggal">Tanggal</th>
                        <th id="td-jumlah" class="middle-table">Jumlah</th>
                        <th id="td-keterangan" class="middle-table">Keterangan</th>
                    </tr>
                    <tr id="tr-content"></tr>
                </table>
            </div>
            <hr/>
            <div class="col-sm form-control-sm" style="margin-top: 15px;">
                <button class="btn btn-primary" id="btn-pdf">Cetak</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        var idJabatan = parseInt('<?php echo $session['id_jabatan']; ?>');
        var IdRuangan = parseInt('<?php echo $session['id_ruangan']; ?>');

        $('#ruangan').val(IdRuangan);
        getNurses(IdRuangan);
        if (idJabatan !== 1) {
            $('#ruangan').prop('disabled', true);
        }

        var trEmpty = '<td class="text-center" colspan="7">Data tidak di temukan</td>';
        $('#table-jadwal #tr-content').append(trEmpty);
    });

    $('#tampil').click(function () {
        var t = $('#tahun').val();
        var b = $('#bulan').val();
        var r = $('#ruangan').val();
        var s = $('#shift').val();
        var n = parseInt($('#nurses').val());

        if (t === '') {
            alert('Tahun harus di isi');
            return false;
        }

        if (b === '') {
            alert('Bulan harus di isi');
            return false;
        }

        $('#jadwal').hide();
        $('#table-jadwal').removeClass('table-responsive');
        $('#table-jadwal #tr-content').html('');
        $('#table-jadwal .tr-user').each(function () {
            $(this).remove();
        });

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "penjadwalan/lapJadwal/",
            data: {
                t: t,
                b: b,
                r: r,
                s: s,
                n: n
            },
            success: function (result) {
                var data = JSON.parse(result);
                var days = parseInt(data.days);

                var trDate = '';

                for (var ti = 1; ti <= days; ti++) {
                    trDate += '<td class="text-center">' + ti + '</td>';
                }

                $('#table-jadwal #tr-content').append(trDate);

                $('#td-no').attr('rowspan', 2);
                $('#td-nama').attr('rowspan', 2);
                $('#td-jabatan').attr('rowspan', 2);
                $('#td-pendidikan').attr('rowspan', 2);
                $('#td-jumlah').attr('rowspan', 2);
                $('#td-keterangan').attr('rowspan', 2);

                $('#td-tanggal').attr('colspan', days);

                var trCntn = $('#table-jadwal tbody').html();
                $('#table-jadwal tbody').html('');

                if (Object.keys(data.nurses).length > 0) {
                    var numTr = 1;
                    var tiB = b.toString();
                    if (tiB.length === 1) {
                        tiB = 0 + tiB;
                    }

                    $.each(data.nurses, function (i, v) {
                        trCntn += '<tr class="tr-user">';
                        trCntn += '<td>' + numTr + '</td>';
                        trCntn += '<td>' + v.nama + '</td>';
                        trCntn += '<td>' + v.nama_jabatan + '</td>';
                        trCntn += '<td>' + v.nama_pendidikan + '</td>';

                        var countHour = 0;
                        for (var ti = 1; ti <= days; ti++) {
                            var tiDate = ti.toString();
                            if (tiDate.length === 1) {
                                tiDate = 0 + tiDate;
                            }
                            var tbTi = t + '-' + tiB + '-' + tiDate;
                            var nurseJadwal = data.nursesJadwal[v.id_nurse];
                            var valJadwal = '';

                            if (nurseJadwal[v.id_nurse + '-' + tbTi] !== null) {
                                var shift = nurseJadwal[v.id_nurse + '-' + tbTi];
                                valJadwal = shift.nama_shift;
                                if (shift.jam_pulang !== null) {
                                    if (shift.jam_masuk !== null) {
                                        var dateStart = '2018-6-12';
                                        var dateEnd = '2018-6-12';
                                        if (shift.jam_masuk > shift.jam_pulang) {
                                            dateStart = '2018-6-11';
                                        }
                                        countHour += diffHours(new Date(dateStart + ' ' + shift.jam_masuk), new Date(dateEnd + ' ' + shift.jam_pulang));
                                    }
                                }
                            }

                            trCntn += '<td>' + valJadwal + '</td>';
                        }

                        trCntn += '<td class="text-center">' + countHour + '</td>';
                        trCntn += '<td></td>';

                        trCntn += '</tr>';
                        numTr++;
                    });
                } else {
                    trCntn += '<tr class="tr-user">';
                    trCntn += '<td class="text-center" colspan="' + (7 + days) + '">Data tidak di temukan</td>';
                    trCntn += '</tr>';
                }

                $('#table-jadwal tbody').append(trCntn);
                $('#table-jadwal').addClass('table-responsive');
                $('#jadwal').show();
            },
            error: function (data) {
                alert(data.statusText);
            }
        });
    });

    function diffHours(dt2, dt1) {
        var diff = (dt2.getTime() - dt1.getTime()) / 1000;
        diff /= (60 * 60);
        return Math.abs(Math.round(diff));
    }

    function getNurses(idRuangan) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "penjadwalan/getNurses/",
            data: {id: idRuangan},
            beforeSend: function () {
                $("#nurses option:gt(0)").remove();
                $('#nurses').find("option:eq(0)").html("Please wait..");
            },
            success: function (data) {
                $('#nurses').find("option:eq(0)").remove();

                var obj = jQuery.parseJSON(data);
                $(obj).each(function ()
                {
                    var option = $('<option />');
                    option.attr('value', this.value).text(this.label);
                    $('#nurses').append(option);
                });

                $('#nurses').val(0);
            },
            error: function (data) {
                alert(data.statusText);
            }
        });
    }

    $('#ruangan').change(function () {
        getNurses($(this).val());
    });

    $('#btn-pdf').click(function () {
        var t = $('#tahun').val();
        var b = $('#bulan').val();
        var r = $('#ruangan').val();
        var s = $('#shift').val();
        var n = $('#nurses').val();

        if (t === '') {
            alert('Tahun harus di isi');
            return false;
        }

        if (b === '') {
            alert('Bulan harus di isi');
            return false;
        }

        var url = '<?php echo base_url(); ?>' + 'penjadwalan/lapJadwalPdf?';
        url += 't=' + t;
        url += '&b=' + b;
        url += '&r=' + r;
        url += '&s=' + s;
        url += '&n=' + n;

        window.open(url);
    });
</script>