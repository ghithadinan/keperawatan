<header id="header">
          <div class="container-fluid">
            <div class="row">
                <div class="col-10 jud">
                  <h6><i class="material-icons" style="font-size:24px;">apps</i>
                 </h6>
                </div>
                <div class="col-2"> </div>
            </div>
          </div>
</header>

<div class="container" style="margin-top: 10px; padding: 10px;">
<div class="card">
  <div class="card-header">
    <h6>Rencana Kegiatan Bulanan Ruangan</h6>
  </div>
  <div class="card-body">

  	<div class="row" style="padding:20px;">

  	<div class="col-sm">
      <h7>Tahun :</h7>
    <select class="form-control form-control-sm" name="tahun" id="tahun">
			      <option value="<?php echo date("Y"); ?>"><?php echo date("Y"); ?></option>
			      <option value="<?php echo date("Y",strtotime("+1 years")); ?>"><?php echo date("Y",strtotime("+1 years")); ?></option>
			    </select>
    </div>

  	<div class="col-sm">
      <h7>Bulan :</h7>
    <select class="form-control form-control-sm" name="bulan" id="bulan">
    			  <option value="a">Bulan</option>
			      <option value="01">Januari</option>
			      <option value="02">Februari</option>
			      <option value="03">Maret</option>
			      <option value="04">April</option>
			      <option value="05">Mei</option>
			      <option value="06">Juni</option>
			      <option value="07">Juli</option>
			      <option value="08">Agustus</option>
			      <option value="09">September</option>
			      <option value="10">Oktober</option>
			      <option value="11">November</option>
			      <option value="12">Desember</option>
	</select>
    </div>

    <div class="col-sm">
    <h7>Ruangan :</h7>
  <select class="form-control form-control-sm" id="ruang" name="ruang" style="width: 140px;">
    <option value="a">Ruangan</option>
    <?php foreach($y->result() as $row):?>
    <option value="<?php echo $row->id_ruangan;?>"><?php echo $row->nama_ruangan;?></option>
    <?php endforeach;?>
  </select>
    </div>

    

    <div class="col-sm form-control-sm" style="margin-top: 15px;">
  <button class="btn btn-primary" id="tampil">Tampil</button>
  		
    </div>
  </div>
  
<table class="table" id="h">
  <th>No</th>
  <th>Bulan</th>
  <th>Ruangan</th>
  <th>Jenis Kegiatan</th>
  <th>Rencana Anggaran</th>
  <th>Rincian</th>
  <tbody class="per">
  <?php $no=1; ?>
  	<?php foreach($x->result() as $row):?>
   <tr>
  	<td><?php echo $no++;?></td>
  	<td><?php echo date("F", strtotime($row->bulan));?></td>
  	<td><?php echo $row->nama_ruangan;?></td>
  	<td><?php echo $row->jenis_kegiatan;?></td>
  	<td><?php echo $row->rencana_anggaran;?></td>
  	<td><?php echo word_limiter($row->rincian,4);?></td>
  </tr>
  <?php endforeach;?>
  </tbody>
</table>
   
  
</div>
</div>

</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#tampil').click(function(){
        	var th=$('#tahun').val();
        	var bl=$('#bulan').val();
            var id=$('#ruang').val();
            $.ajax({
                url : "<?php echo base_url();?>arencana/bul",
                method : "POST",
                data : {th: th,bl: bl,id: id},
                dataType : 'json',
                success: function(data){
                   var html = '';
                    var i;
                    var u=1;
                    for(i=0; i<data.length; i++){
                        html+='<tr>'
                        +'<td>'+u+++'</td>'
                        +'<td>'+data[i].bulan+'</td>'
                        +'<td>'+data[i].jenis_kegiatan+'</td>'
                        +'<td>'+data[i].nama_ruangan+'</td>'
                        +'<td>'+data[i].rencana_anggaran+'</td>'
                        +'<td>'+data[i].rincian+'</td>'
                        +'<td>'+'<a href="latih/show_p/'+data[i].id_nurse+'" class="badge badge-primary">Detail</a>'+'</td>'+
                        '</tr>';
                    }
                    $('.per').html(html);
                     
                }
            });
        });
    });
</script>
