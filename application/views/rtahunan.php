<header id="header">
          <div class="container-fluid">
            <div class="row">
                <div class="col-10 jud">
                  <h6><i class="material-icons" style="font-size:24px;">apps</i>
                  <?php echo $judul ?></h6>
                </div>
                <div class="col-2"> </div>
            </div>
          </div>
</header>
<div class="container" style="margin-top: 10px; padding: 10px;">
<div class="card">
  <div class="card-header">
    <h6>Input Rencana Kegiatan Tahunan Ruangan</h6>
  </div>
  <div class="card-body">
    <?php echo form_open('rencana/input_rtahunan'); ?>
	    <div class="form-row">
	    	<div class="col-sm-6">
	    		<div class="form-group">
				    <label for="judul">Jenis Kegiatan</label>
				    <input type="text" class="form-control form-control-sm " name="jenis_kegiatan">
				</div>
			</div>
		</div>
		<div class="form-row">
		<div class="col-sm-3">
			<div class="form-group input-group input-group-sm mb-3">
			    <label for="judul">Rencana Anggaran</label>
			    <div class="input-group-prepend">
          			<div class="input-group-text" id="inputGroup-sizing-sm">Rp.</div>
          			<input type="text" class="form-control uang form-control-sm " name="anggaran">
       			 </div>   
			</div>
		</div>
		</div>
		<div class="form-row">
		<div class="col-sm-2">
			<div class="form-group">
			    <label for="judul">Tahun</label>
			    <select class="form-control form-control-sm" name="tahun">
			      <option value="<?php echo date("Y"); ?>"><?php echo date("Y"); ?></option>
			      <option value="<?php echo date("Y",strtotime("+1 years")); ?>"><?php echo date("Y",strtotime("+1 years")); ?></option>
			    </select>
			</div>
		</div>
		</div>
		<div class="form-row">
		<div class="col-sm-12">
			<div class="form-group">
			    <label for="isi">Rincian/Keterangan</label>
			    <textarea class="form-control" name="isi" id="editor1"></textarea>
			</div>
		</div>
		</div>
		<div class="form-row">
			<div class="col-sm-12">
			  <button type="submit" name="lharian" class="btn btn-success">Simpan</button>
			  <button type="reset" class="btn btn-danger">Cancel</button>
			</div>
		</div>
	<?php echo form_close(); ?>
  
</div>
</div>

</div>
<script type="text/javascript">
	CKEDITOR.replace('editor1');
</script>

