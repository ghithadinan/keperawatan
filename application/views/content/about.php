<div class="container" style="margin-top: 10px">
<div class="alert alert-secondary alert-dismissible fade show" role="alert">
  <h6 class="alert-heading">Profil Individu</h6>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
</div>
<style type="text/css">
  .table thead tr th, .table tbody tr td {
    border: none;
}
</style>
<div class="container">
      <div class="card">
        <?php foreach($records as $ind ): ?>
        <img src="<?php echo base_url();?>assets/uploads/files/<?php echo $ind->image; ?>" alt="Card image cap" style="border-radius: 8px; width: 180px; height: 240px; border: 1px solid #ddd; padding: 5px; display: block; margin-left: auto; margin-right: auto; margin-top: 10px;">
        <span style="display: block; margin-left: auto; margin-right: auto;">
          <?php 
                  echo $ind->gelar_depan." ".$ind->nama.", ".$ind->gelar_belakang;
            endforeach; ?> </span>
        <div class="card-body">

                  <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Profil</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Riwayat Pendidikan</a>
          </li> -->
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

            <table class="table">
              <tr >
                <td style="width: 20%">NIP</td>
                <td style="width: 25%"><?php foreach($records as $ind ) {
                  echo $ind->nip;
                } ?> </td>
                <td style="width: 55%"></td>
              </tr>
               <tr>
                <td>Nama</td>
                <td><?php echo $ind->nama; ?></td>
                <td></td>
              </tr>
              <tr>
                <td>Gelar Belakang</td>
                <td><?php echo $ind->gelar_belakang; ?></td>
                <td></td>
              </tr>
              <tr>
                <td>Tgl Lahir</td>
                <td><?php echo date('d-m-Y',strtotime($ind->tanggal_lahir)); ?></td>
                <td></td>
              </tr>
              <tr>
                <td>Status Kepegawaian</td>
                <td><?php echo $ind->status_kepegawaian; ?></td>
                <td></td>
              </tr>
              <tr>
                <td>Tugas Pokok</td>
                <td><?php echo $ind->nama_pk; ?></td>
                <td></td>
              </tr>
              <tr>
                <td>Pangkat/Gol</td>
                <td><?php echo $ind->pangkat." (".$ind->nama_gol.")"; ?></td>
                <td></td>
              </tr>
              <tr>
                <td>Jabatan</td>
                <td><?php echo $ind->nama_jabatan; ?></td>
                <td></td>
              </tr>
               <tr>
                <td>Ruangan</td>
                <td><?php echo $ind->nama_ruangan; ?></td>
                <td></td>
              </tr>
              <tr>
                <td>Jenis Kelamin</td>
                <td><?php if($ind->jk==1){echo "Laki-laki";}else{echo "Perempuan";} ?></td>
                <td></td>
              </tr>
            </table>


          </div>
          <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">




          </div>
        </div>
        </div>
      </div>
</div>

