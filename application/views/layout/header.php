<!DOCTYPE html>
  <html>
    <head>
        <title>E-Nursing | RSUD Ciamis</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/rsu-logo.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"  />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css"  />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/vendor/toastr/toastr.min.css"  />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/vendor/select2/css/select2.min.css"  />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css"  />

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="<?php echo base_url();?>assets/vendor/moment/moment.min.js""></script>

        <script src="<?php echo base_url();?>assets/vendor/bootbox/bootbox.min.js" ></script>
        <script src="<?php echo base_url();?>assets/vendor/toastr/toastr.min.js" ></script>
        <script src="<?php echo base_url();?>assets/vendor/select2/js/select2.full.min.js" ></script>
        <script src="<?php echo base_url();?>assets/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js" ></script>
        <script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
        <script src="https://cdn.rawgit.com/igorescobar/jQuery-Mask-Plugin/1ef022ab/dist/jquery.mask.min.js"></script>

        <?php
            if(!empty($css_files)):
                foreach($css_files as $file): ?>
                    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
                <?php endforeach;
            endif;
        ?>

     <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
     <style type="text/css">
       .material-icons{
        vertical-align: middle;
       }
     </style>
    </head>

    <body>
          <div class="container">
            <div class="row" id="h""> 
              <div class="col-md-1 text-center" id="rr">
                <img src="<?php echo base_url();?>assets/img/rsu-logo.png" style="width: 90px; padding: 5px;">
              </div>
              <div class="col-md-4 text-center" id="rr" style="padding-top: 15px;">
                <h4 style="font-family: 'Segoe UI_', 'Open Sans', Verdana, Arial, Helvetica, sans-serif; letter-spacing: -2px; color: #7a378b;">
                   Sistem Informasi e-Nursing
                </h4>
                <h4 style="font-family: 'Blackadder ITC'; color: #7a378b;">Rumah Sakit Umum Daerah Ciamis</h4>
              </div>
            </div>
          </div>
            <div class="row" style="margin-bottom: 5px;">
              <div class="col-md-12" style="padding-right: 0px">
           
          <nav class="navbar navbar-expand-md navbar-dark" style="background-color: #7a378b;">
           <!--  <a class="navbar-brand" href="#">E-Nursing | RSUD Ciamis</a> -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <?php if(!$this->session->userdata('logged_in')): ?>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>users/index">Login</a>
              </li>
            <?php endif; ?>
            <?php if($this->session->userdata('logged_in') AND $this->session->userdata('jabatan')=='Kepala Bidang'): ?>
               <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <i class="material-icons">input</i>&nbsp;Input Data
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('perawat/show_perawat/add')?>">Data Perawat</a>
               <!--  <div class="dropdown-divider"></div> -->
                  <!-- <a class="dropdown-item" href="<?php //echo site_url('logbook/add')?>">Data Logbook</a> -->
              </li>
               <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <i class="material-icons">book</i>Laporan
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('perawat/show_perawat')?>">Data Perawat</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('penjadwalan/laporan')?>">Data Jadwal</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('logbook/report')?>">Data Logbook</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">folder_open</i>&nbsp;Master
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('master/golongan')?>">Golongan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('master/jabatan')?>">Jabatan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('master/ruangan')?>">Ruangan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('master/pendidikan')?>">Pendidikan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('master/activity')?>">Kegiatan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('master/activityPk')?>">Kegiatan PK</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Perijinan
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('perijinan/read_simk')?>">SIM-K</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('perijinan/str')?>">STR</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('perijinan/sipp')?>">SIPP</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <i class="material-icons">print</i>&nbsp;Print
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('cetaks/index')?>">Status Kepegawaian</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('cetaks/laporan')?>">Golongan Ruang</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('cetaks/kualifikasi')?>">Kualifikasi</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>charts/index"><i class="material-icons">pie_chart</i>&nbsp;Charts</a>
              </li>
              <?php endif; ?>
               <?php if($this->session->userdata('logged_in') AND ($this->session->userdata('id_jabatan')==5 || $this->session->userdata('id_jabatan')==6) ): ?>
               <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Laporan
                </a>
                 <div class="dropdown-menu" aria-labelledby="navbarDropdown"><!--
                  <a class="dropdown-item" href="<?php // echo site_url('laporan/harian')?>">Harian</a>--><?php //endif; ?> 
                 
                  <!-- <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php //echo site_url('laporan/bulanan')?>">Bulanan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php //echo site_url('laporan/tahunan')?>">Tahunan</a> -->
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('logbook/report')?>">Logbook</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('penjadwalan/laporan')?>">Jadwal</a>
                  </div></li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Input Data
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('penjadwalan/add')?>">Data Jadwal</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('logbook/add')?>">Data Logbook</a>
                </div>
              </li>
                 <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Rencana Kegiatan
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php // echo site_url('rencana/bulanan')?>">Bulanan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php //echo site_url('rencana/tahunan')?>">Tahunan</a>
                </div> -->

                <?php endif; ?>
              
              <?php if($this->session->userdata('logged_in')): ?>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

        <img src="<?php echo base_url();?>assets/uploads/files/<?php echo $this->session->userdata("img");?>" alt="Card image cap" style="width: 15px; height: 20px; border: 1px solid #ddd;">
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo base_url(); ?>perawat/about">
                  <i class="material-icons" style="vertical-align: middle;">account_circle</i>Profile</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo base_url(); ?>users/logout"><i class="material-icons" style="vertical-align: middle;">
power_settings_new
</i>Logout</a>
                </div>
              </li>
              <?php endif; ?>
            </ul>
            <!--<form class="form-inline my-2 my-lg-0">-->
              <!--<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>-->
            <!--</form>-->
          </div>
        </nav>
        </div>
 </div>


        <?php if($this->session->flashdata('login_failed')):?>
        <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>';?>
        <?php endif; ?>
