<!DOCTYPE html>
  <html>
    <head>
      <title>E-Nurse | RSUD Ciamis</title>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
      <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"  />
      <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css"  />

  
     <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
     <script type="text/javascript">
   google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart()
{
  var data = google.visualization.arrayToDataTable([
    ['Gender','Number'],
    <?php foreach($genders as $gender){
      echo "['".$gender->nama_jk."',".$gender->jk."],";
    }
    ?>
    ]);
  var options = {
      title : 'Porsentase Jenis Kelamin'
  };
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data,options);
}
 </script>
 <script type="text/javascript">
   google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart()
{
  var data = google.visualization.arrayToDataTable([
    ['Golongan','Jumlah'],
    <?php foreach($gols as $gol){
      echo "['".$gol->nama_gol."',".$gol->gol."],";
    }
    ?>
    ]);
  var options = {
      title : 'Porsentase Golongan'
  };
  var chart = new google.visualization.PieChart(document.getElementById('piecharm'));
  chart.draw(data,options);
}
 </script>
  <script type="text/javascript">
   google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart()
{
  var data = google.visualization.arrayToDataTable([
    ['Golongan','Jumlah'],
    <?php foreach($pks as $pk){
      echo "['".$pk->nama_pk."',".$pk->pk."],";
    }
    ?>
    ]);
  var options = {
      title : 'PK'
  };
  var chart = new google.visualization.PieChart(document.getElementById('piechars'));
  chart.draw(data,options);
}
 </script>
    </head>

    <body>

           <div class="container-fluid" id="h" style="background-color: white;"><div class="row" id="rr" style="height: 70px;"> <div class="col-1" id="rr"><img src="<?php echo base_url();?>assets/img/rsu-logo.png" style="width: 60px; padding: 5px;"></div><div class="col" id="rr"><h3 style="margin-top:20px; font-family: Times New Roman; color: #7a378b;">Sistem Informasi e-Nursing RSUD Ciamis</h3></div></div></div>
          <nav class="navbar navbar-expand-md navbar-dark" style="background-color: #7a378b;">
           <!--  <a class="navbar-brand" href="#">E-Nursing | RSUD Ciamis</a> -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <?php if(!$this->session->userdata('logged_in')): ?>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>users/index">Login</a>
              </li>
            <?php endif; ?>
            <?php if($this->session->userdata('logged_in') AND $this->session->userdata('jabatan')=='Kepala Bidang'): ?>
               <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Input Data
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('perawat/show_perawat/add')?>">Data Perawat</a>
                <div class="dropdown-divider"></div>
                  <!-- <a class="dropdown-item" href="<?php echo site_url('logbook/add')?>">Data Logbook</a> -->
              </li>
               <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Laporan
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('perawat/show_perawat')?>">Data Perawat</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('penjadwalan/laporan')?>">Data Jadwal</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('logbook/report')?>">Data Logbook</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Master
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('master/golongan')?>">Golongan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('master/jabatan')?>">Jabatan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('master/ruangan')?>">Ruangan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('master/pendidikan')?>">Pendidikan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('master/activity')?>">Kegiatan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('master/activityPk')?>">Kegiatan PK</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Perijinan
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('perijinan/read_simk')?>">SIM-K</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('perijinan/str')?>">STR</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('perijinan/sipp')?>">SIPP</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Print
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('cetaks/index')?>">Status Kepegawaian</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('cetaks/laporan')?>">Golongan Ruang</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('logbook/report')?>">Data Logbook</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>charts/index">Charts</a>
              </li>
              <?php endif; ?>
               <?php if($this->session->userdata('logged_in') AND $this->session->userdata('jabatan')=='Kepala Ruangan'): ?>
               <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Laporan
                </a>
                 <div class="dropdown-menu" aria-labelledby="navbarDropdown"><!--
                  <a class="dropdown-item" href="<?php // echo site_url('laporan/harian')?>">Harian</a>--><?php //endif; ?> 
                 
                  <!-- <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php //echo site_url('laporan/bulanan')?>">Bulanan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php //echo site_url('laporan/tahunan')?>">Tahunan</a> -->
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('logbook/report')?>">Logbook</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('penjadwalan/laporan')?>">Jadwal</a>
                  </div></li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Input Data
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo site_url('penjadwalan/add')?>">Data Jadwal</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('logbook/add')?>">Data Logbook</a>
                </div>
              </li>
                 <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 Rencana Kegiatan
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php // echo site_url('rencana/bulanan')?>">Bulanan</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php //echo site_url('rencana/tahunan')?>">Tahunan</a>
                </div> -->

                <?php endif; ?>
              
              <?php if($this->session->userdata('logged_in')): ?>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

        <img src="<?php echo base_url();?>assets/uploads/files/<?php echo $this->session->userdata("img");?>" alt="Card image cap" style="width: 30px; height: 30px; border: 1px solid #ddd;">
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo base_url(); ?>perawat/about">Profile</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo base_url(); ?>users/logout">Logout</a>
                </div>
              </li>
              <?php endif; ?>
            </ul>
            <!--<form class="form-inline my-2 my-lg-0">-->
              <!--<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>-->
            <!--</form>-->
          </div>
        </nav>



        <?php if($this->session->flashdata('login_failed')):?>
        <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>';?>
        <?php endif; ?>


       



<div class="container" style="margin-top: 20px; padding: 10px;">
 
 <div style="height: 400px" id="piechart"> </div>
 <div style="height: 400px" id="piecharm"> </div>
 <div style="height: 400px" id="piechars"> </div>

</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


    </body>
  </html>