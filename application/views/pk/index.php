<div class="container" id="content">
    <div class="card">
        <div class="card-header bg-info text-white" style="padding: 4px 1.25rem; ">
            Penjadwalan
        </div>
        <div class="card-body">
            <form class="form" id="form-filter">
                <div class="row" style="padding:20px;">
                    <div class="col-sm">
                        <h7>PK :</h7>
                        <select class="form-control form-control-sm input-select2" name="pk_id">
                            <option value="">-</option>
                            <?php foreach ($pk as $dt): ?>
                                <option value="<?php echo $dt->id_pk; ?>"><?php echo $dt->nama_pk; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-sm form-control-sm" style="margin-top: 15px;">
                        <button type="submit" class="btn btn-primary">Tampil</button>
                    </div>
                </div>
            </form>
            <div class= "row" id="view-activity" style="display: none;">
                <hr style="margin-top: 0px;margin-bottom: 0px;">
                <div class="col-md-12">
                    <form id="form-activity">
                        <table class="table table-consended" id="table-activity" width="100%">
                            <thead>
                                <tr>
                                    <th colspan="2">List Kegiatan dari : <span class="text-pkName"></span></th>
                                    <td class="text-right"><button type="button" data-toggle="modal" data-target="#formActivityModal" class="btn btn-success">Tambah Kegiatan</button></td>
                                </tr>
                                <tr style="background: aliceblue;">
                                    <th class="text-center" style="width:10px;">No</th>
                                    <th style="width: 500px">Nama Kegiatan</th>
                                    <th style="width: 50px" class="text-right"></th>
                                </tr>
                            </thead>
                            <tbody class="empty" style="display: none;">
                                <tr class="tr-empty">
                                    <td colspan="3">
                                        <div class="alert alert-warning text-center">Belum ada data</div>
                                    </td>
                                </tr>
                            </tbody>
                            <tbody class="list"></tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="formActivityModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Kegiatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-activity">
                <div class="modal-body">
                    <div class="form-group">
                        <select name="activity_id" class="form-control input-select2" multiple="true"></select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var elContent       = $('#content');
    var elFormFilter    = $('form#form-filter', elContent);
    var elTableActivity = $('table#table-activity');

    var elFormActivity = $('form#form-activity');

    var _pk = JSON.parse('<?php echo json_encode($pk) ?>');
    var activities = JSON.parse('<?php echo json_encode($activities) ?>');
    var acIds = [];

    var selectedPkId = '' 

    elFormFilter.submit(function(event) {
        event.preventDefault();

        var _this   = $(this)
        var pkId = $(':input[name="pk_id"]', _this).val()
       
        if(! (pkId)) {
            alert('input tidak lengkap');
            return false;
        }

        getDataByPk(pkId)
    });

    $(elTableActivity).on('click', '.btn-delete', function(event) {
        event.preventDefault();
        var _this = $(this)
        bootbox.confirm("Anda yakin menghapus kegiatan ini ?", function(isConfirm) {
            if(isConfirm) {

                var formData = {
                    ac_id: _this.closest('tr').attr('data-ac_id'),
                    pk_id: _this.closest('tr').attr('data-pk_id')
                }

                $.post('<?php echo base_url() ?>'+'master/activityPkDetailDelete', formData, function(data) {
                    _this.closest('tr').remove()
                    getDataByPk(selectedPkId)
                });
            }
        });
    });

    function getDataByPk(pkId) {

        var formData = {pk_id: pkId}
        var selectedPk = _pk.find(function(item) { return item.id_pk == pkId })
        
        if(selectedPk) {
            $('.text-pkName').html(selectedPk.nama_pk)
        }

        $.get('<?php echo base_url() ?>'+'master/activityPkDetail', formData, function(data) {
            elTableActivity.find('tbody.list > tr').empty();
            $(elTableActivity).find('tbody.empty').hide()

            let acIds = []
            let no = 1
            $.each(data.listData, function(index, val) {

                acIds.push(val.ac_id)                

                let html = '<tr data-ac_id="'+val.ac_id+'" data-pk_id="'+val.pk_id+'">';
                html += '<td class="text-center">';
                html += '<span class="counter">'+(no++)+'</span>';
                html += '</td>';
                html += '<td>';
                html += '<span class="text-activityName">'+val.activity_name+'</span>';
                html += '</td>';
                html += '<td class="text-right">';
                html += '<button type="button" class="btn btn-sm btn-danger btn-delete">Hapus</button>';
                html += '</td>';
                html += '</tr>';

                elTableActivity.find('tbody.list').append(html)
            });

            if(! data.listData.length) {
                $(elTableActivity).find('tbody.empty').show()
            } 

            renderOptActivity(acIds)

            selectedPkId = pkId

            $('#view-activity').show()
        });
    }

    function renderOptActivity(acIds) {
        var list = activities 
        if(acIds && acIds.length) {
            list = list.filter(function(item) { return ! ($.inArray(item.id, acIds) > -1); })
        }

        var elSelect = $(':input[name="activity_id"]', elFormActivity);
        elSelect.empty();
        // elSelect.append('<option value=""></option>');

        $.each(list, function(index, val) {
            elSelect.append('<option value="'+val.id+'">'+val.name+'</option>');
        });
    }

    $(elFormActivity).submit(function(event) {
        event.preventDefault()
        var _this = $(this)
        var acId  = $(':input[name="activity_id"]', _this).val()

        if(! acId) {
            alert('Input belum lengkat')
            return false;
        }

        var formData = {
            activity_id : acId,
            pk_id: selectedPkId
        }

        $.post('<?php echo base_url() ?>'+'master/activityPkDetailCreate', formData, function(data) {
            $('.modal').modal('hide')
            getDataByPk(selectedPkId)
        });
    });

    $(':input.input-select2').select2({
        width:'100%'
    });
</script>