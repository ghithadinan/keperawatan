<div class="container">
    <div class="card">
        <div class="card-header bg-info text-white" style="padding: 4px 1.25rem; ">
            Penjadwalan
        </div>
        <div class="card-body">
            <form class="form" id="form-filter">
                <div class="row" style="padding:20px;">
                        <div class="col-sm">
                            <h7>Nama Perawat :</h7>
                            <select class="form-control form-control-sm input-select2" name="nurse_id">
                                <option value="">-</option>
                                <?php foreach ($nurse as $dt): ?>
                                    <option value="<?php echo $dt->id_nurse; ?>"><?php echo $dt->nama; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm">
                            <h7>Tanggal :</h7>
                            <div class="input-group mb-3">
                                <input type="text" name="date" class="form-control form-control-sm input-datepicker">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm form-control-sm" style="margin-top: 15px;">
                            <button type="submit" class="btn btn-primary">Tampil</button>
                        </div>
                </div>
            </form>
            <div class= "row" id="view-booklog" style="display: none;">
                <hr style="margin-top: 0px;margin-bottom: 0px;">
                <div class="col-md-12">
                    <table class="table table-consended" id="table-nurse">
                        <tbody>
                            <tr>
                                <th style="width: 150px;">Nama</th>
                                <td>: <span class="text-nurseName"></span></td>
                            </tr>
                            <tr>
                                <th>Ruangan</th>
                                <td>: <span class="text-roomName"></span></td>
                            </tr>
                            <tr>
                                <th>Tanggal</th>
                                <td>: <span class="text-date"></span></td>
                            </tr>
                            <tr>
                                <th>Kualifikasi (PK)</th>
                                <td>: <span class="text-pkName"></span></td>
                            </tr>
                            <tr>
                                <th>Jadwal Shift</th>
                                <td>: <span class="text-shiftName"></span></td>
                            </tr>
                        </tbody>
                    </table>
                    <hr style="margin-top: 0px;margin-bottom: 0px;">
                    <form id="form-booklog">
                        <table class="table table-consended" id="table-booklog">
                            <thead>
                                <tr>
                                    <th class="align-middle" style="width:100px;">Jam</th>
                                    <th style="width: 500px">Nama Kegiatan</th>
                                    <th style="width: 500px">Nama Pasien</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var elFormBooklog = $('form#form-booklog');
    var elFormFilter  = $('form#form-filter');
    var elTableLog    = $('table#table-booklog');
    var elTableNurse  = $('table#table-nurse');

    elFormFilter.submit(function(event) {
        event.preventDefault();
        var _this = $(this)
        var nurseId = $(':input[name="nurse_id"]', _this).val()
        var date    = $(':input[name="date"]', _this).val()

        if(! (nurseId && date)) {
            alert('input tidak lengkap');
            return false;
        }

        $.get('<?php echo base_url() ?>'+'logbook/getDetail', _this.serialize(), function(data) {

            $('#view-booklog').show()

            $.each(data.info, function(index, val) {
                 $('.text-'+ index, elTableNurse).html(val)
            });

            elTableLog.find('tbody > tr').empty();
            $.each(data.listData, function(index, val) {
                var dt = '<tr>'
                dt += '<td>';
                dt += '<input type="hidden" name="id[]" value="">';
                dt += '<input type="hidden" name="time[]" value="'+index+'">';
                dt += '<input type="hidden" name="date[]" value="'+val.date+'">';
                dt += '<input type="hidden" name="nurse_id[]" value="'+val.nurse_id+'">';
                dt += '<input type="hidden" name="pk_id[]" value="'+val.pk_id+'">';
                dt += '<input type="hidden" name="shift_id[]" value="'+val.shift_id+'">';
                dt += index+'</td>';
                dt += '<td>';
                dt += '<select name="activity_id[]" class="form-control input-select2">';
                dt += '<option value="">-</option>';

                $.each(data.activity, function(idx, value) {
                    var selected = ''

                    if(parseInt(val.activity_id) === parseInt(value.id)) {
                        selected = 'selected'
                    }

                    dt += '<option value="'+value.id+'" '+selected+'>'+value.name+'</option>';
                });

                dt += '</select>';
                dt += '</td>';
                dt += '<td> <input type="text" class="form-control" name="patient_name[]" value="'+val.patient_name+'"></td>';
                dt += '</tr>';

                elTableLog.find('tbody').append(dt)
            });

            $(':input.input-select2', elTableLog).select2();
        });
    });

    elFormBooklog.submit(function(event) {
        event.preventDefault()
        var _this = $(this);
        $.post('<?php echo base_url('logbook/save') ?>', _this.serialize(), function(data, textStatus, xhr) {
            event.preventDefault();
            toastr.success(data.message, 'Success')
        }).fail(function() {
            toastr.error('Terjadi Kesalahan', 'Gagal')
        });;
    });

    $(':input.input-datepicker').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    $(':input.input-select2').select2();
</script>
