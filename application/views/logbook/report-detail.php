<div class="modal-header">
    <h5 class="modal-title">Detail</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <table class="table table-consended">
        <tbody>
            <tr>
                <th style="width: 100px; border-top: none;">Tanggal</th>
                <th style="border-top: none;">: <?php echo $date ?></th>
            </tr>
        </tbody>
    </table>

    <table class="table table-consended table-bordered">
        <thead>
            <tr>
                <th style="width: 50px">Jam</th>
                <th>Nama Kegiatan</th>
                <th>Nama Pasien</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data as $dt): ?>
                <tr>
                    <td><?php echo substr($dt->time, 0, 5) ?></td>
                    <td><?php echo !empty($dt->activity) ? $dt->activity->name : '-' ?></td>
                    <td><?php echo $dt->patient_name ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>
