<div class="container" id="content">
    <div class="card">
        <div class="card-header bg-info text-white" style="padding: 4px 1.25rem; ">
            Logbook
        </div>
        <div class="card-body">
            <form class="form" id="form-filter" method="post">
                <div class="row" style="padding:20px;">
                    <div class="col-sm">
                        <h7>Tahun :</h7>
                        <select class="form-control form-control-sm input-select2" name="year">
                            <option value="">-</option>
                            <?php foreach ($optYear as $key => $dt): ?>
                                <option value="<?php echo $key ?>" <?php if($this->input->post('year') == $key): echo 'selected'; endif; ?>><?php echo $dt ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-sm">
                        <h7>Bulan :</h7>
                        <select class="form-control form-control-sm input-select2" name="month">
                            <option value="">-</option>
                             <?php foreach ($optMonth as $key => $dt): ?>
                                <option value="<?php echo $key ?>" <?php if($this->input->post('month') == $key): echo 'selected'; endif; ?>><?php echo $dt ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <?php if ($this->session->userdata('jabatan')=='Kepala Bidang'): ?>
                        <div class="col-sm">
                            <h7>Ruangan :</h7>
                            <select class="form-control form-control-sm input-select2" name="room_id">
                                <option value="">-</option>
                                 <?php foreach ($rooms as $dt): ?>
                                    <option value="<?php echo $dt->id_ruangan ?>"><?php echo $dt->nama_ruangan ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    <?php endif ?>
                    <div class="col-sm">
                        <h7>Nama :</h7>
                        <select class="form-control form-control-sm input-select2" name="nurse_id">
                            <option value="">-</option>
                            <?php foreach ($nurses as $dt): ?>
                                <option value="<?php echo $dt->id_nurse; ?>"  <?php if($this->input->post('nurse_id') == $dt->id_nurse): echo 'selected'; endif; ?>><?php echo $dt->nama; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-sm form-control-sm" style="margin-top: 15px;">
                        <button class="btn btn-primary" id="tampil">Tampil</button>
                    </div>
                </div>
            </form>
            <?php if (!empty($listData)): ?>
                <hr style="margin-top: 0px;margin-bottom: 0px;">
                <div class="row" id="view-activity">
                    <table class="table table-consended" id="table-nurse">
                        <tbody>
                            <tr>
                                <th style="width: 150px;">Nama</th>
                                <td>: <span class="text-nurseName"><?php echo $info['nurseName'] ?></span></td>
                            </tr>
                            <tr>
                                <th>Ruangan</th>
                                <td>: <span class="text-roomName"><?php echo $info['roomName'] ?></span> </td>
                            </tr>
                            <tr>
                                <th>Bulan</th>
                                <td>: <span class="text-monthName"><?php echo str_pad($info['month'], 2, '0', STR_PAD_LEFT).'/'.$info['year'] ?></span></td>
                            </tr>
                            <tr>
                                <th>Kualifikasi</th>
                                <td>: <span class="text-pkName"><?php echo $info['pkName'] ?></span></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="table-activity">
                            <thead>
                                <tr>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">Kegiatan</th>
                                    <th colspan="<?php echo $dayCount ?>" class="justify-content-center align-items-center text-center">Tanggal</th>
                                </tr>
                                <tr>
                                    <?php for ($i=1; $i <= $dayCount; $i++): ?>
                                        <th class="align-middle text-center"><?php echo $i ?></th>
                                    <?php endfor; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1 ?>
                                <?php foreach ($listData as $dt): ?>
                                    <tr data-activity_id="<?php echo $dt['activity_id'] ?>">
                                        <td><?php echo $no++ ?></td>
                                        <td><?php echo $dt['activity_name'] ?></td>
                                        <?php for ($i=1; $i <= $dayCount; $i++): ?>
                                            <td class="align-middle">
                                                <?php if(!empty($dt['days'][$i]) && !empty($dt['days'][$i]['value'])): ?>
                                                    <a href="javascript:;" data-day="<?php echo $i ?>" class="btn-detail-day"><?php echo !empty($dt['days'][$i]) && !empty($dt['days'][$i]['value']) ? $dt['days'][$i]['value'] : '-' ?></a>
                                                <?php else: ?>
                                                    -
                                                <?php endif; ?>
                                            </td>
                                        <?php endfor; ?>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>

<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>

<script type="text/javascript">

    var elContent = $('#content');
    var elFormFilter = $('form#form-filter', elContent);

    elFormFilter.submit(function(event) {
        var _this   = $(this)
        var nurseId = $(':input[name="nurse_id"]', _this).val()
        var month   = $(':input[name="month"]', _this).val()
        var year    = $(':input[name="year"]', _this).val()

        if(! (nurseId && month && year)) {
            alert('input tidak lengkap');
            return false;
        }

        return true
    });

    $(':input[name="room_id"]', elFormFilter).change(function(event) {
        event.preventDefault()
        var _this = $(this)
        var value = _this.val()

        $(':input[name="nurse_id"]', elFormFilter).val('').trigger('change');
        $(':input[name="nurse_id"]', elFormFilter).html('<option value="">-</option>');
        $(':input[name="nurse_id"]', elFormFilter).attr('disabled', true);

        if(! value)
            return;

        $.get('<?php echo base_url('logbook/getListNurseByRoom') ?>/'+ value, function(data) {
            var list = data.data

            if(list.length) {
                $(':input[name="nurse_id"]', elFormFilter).removeAttr('disabled')

                $.each(list, function(index, val) {
                     $(':input[name="nurse_id"]', elFormFilter).append('<option value="'+val.id_nurse+'">'+val.nama+'</option>')
                });
            }
        });
    });

    $('.btn-detail-day', elContent).click(function(event) {
        event.preventDefault();
        var formData = {
            day: $(this).data('day'),
            month: '<?php echo $this->input->post('month') ?>',
            year: '<?php echo $this->input->post('year') ?>',
            nurse_id: '<?php echo $this->input->post('nurse_id') ?>',
            activity_id: $(this).closest('tr').data('activity_id')
        };

        $.post('<?php echo base_url().'/logbook/reportDetail' ?>' , formData, function(data, textStatus, xhr) {
            $('#detailModal').find('.modal-content').html(data)
            $('#detailModal').modal('show')
        });
    });

    $(':input.input-select2').select2();
</script>
