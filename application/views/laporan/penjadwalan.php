<html>
    <head>
        <title><?php echo $title; ?></title>
        <style>
            .logo {
                padding-bottom: 5px;
                width: 375px;
            }

            .heading {
                background: #F0F0E9;
                color: #363432;
                font-size: 20px;
                margin-bottom: 20px;
                padding: 10px 25px;
                font-family: 'Roboto', sans-serif;
                text-align: center;
            }

            .table-data {
                width: 100%;
                border-collapse: collapse;
            }

            .content-table {
                border: 1px solid black;
                padding: 0.5em 0.5em;
            }

            .center {
                text-align: center;
            }

            .money {
                text-align: right;
            }
            #liburs{
                background-color: red;
            }
        </style>
    </head>
    <body>
        <div class="logo">
        </div>
        <h2 class="heading"><?php echo $title; ?></h2>
        <?php echo!empty($post['t']) ? '<h4>Tahun : ' . $post['t'] . '</h4>' : ''; ?>
        <?php echo!empty($bulan) ? '<h4>Bulan : ' . $bulan . '</h4>' : ''; ?>
        <?php echo!empty($shift->nama_shift) ? '<h4>Shift : ' . $shift->nama_shift . '</h4>' : ''; ?>
        <?php echo!empty($ruangan->nama_ruangan) ? '<h4>Ruangan : ' . $ruangan->nama_ruangan . '</h4>' : ''; ?>
        <?php echo!empty($nurse->nama) ? '<h4>Nama : ' . $nurse->nama . '</h4>' : ''; ?>
        <table class="table-data">
            <tr style="background: #DADADA;" class="content-table">
                <th class="content-table" rowspan="2">No</th>
                <th class="content-table" rowspan="2" width="150px">Nama</th>
                <th class="content-table" rowspan="2" width="200px">Jabatan</th>
                <th class="content-table" rowspan="2">Pendidikan</th>
                <th class="content-table" colspan="<?php echo $jadwal['days']; ?>">Tanggal</th>
                <th class="content-table" width="150px">Jumlah</th>
                <th class="content-table" rowspan="2">Keterangan</th>
            </tr>
            <tr style="background: #DADADA;" class="content-table">
                <?php for ($id = 1; $id <= $jadwal['days']; $id++): ?> <th class="content-table"><?php echo $id; ?></th> <?php endfor; ?><th>Jam Kerja</th>
            </tr>
            <?php
            if (count($jadwal['nurses']) > 0):
                $no = 1;
                foreach ($jadwal['nurses'] as $nurse) :
                    ?>
                    <tr>
                        <td class="content-table center"><?php echo $no; ?></td>
                        <td class="content-table"><?php echo!empty($nurse->nama) ? $nurse->nama : ''; ?></td>
                        <td class="content-table"><?php echo!empty($nurse->nama_jabatan) ? $nurse->nama_jabatan : ''; ?></td>
                        <td class="content-table"><?php echo!empty($nurse->nama_pendidikan) ? $nurse->nama_pendidikan : ''; ?></td>
                        <?php
                        $bulan = $post['b'];
                        $bulanV = $bulan;
                        if (strlen((String) $bulan) == 1) {
                            $bulanV = 0 . $bulan;
                        }

                        $countHour = 0;
                        for ($id = 1; $id <= $jadwal['days']; $id++) {
                            $daysV = $id;
                            if (strlen((String) $id) == 1) {
                                $daysV = 0 . $id;
                            }
                            $thnBlnHari = $post['t'] . '-' . $bulanV . '-' . $daysV;
                            $nurseJadwal = $jadwal['nursesJadwal'][$nurse->id_nurse];
                            $jadwalV = '';

                            if ($nurseJadwal[$nurse->id_nurse . '-' . $thnBlnHari] != null) {
                                $shift = $nurseJadwal[$nurse->id_nurse . '-' . $thnBlnHari];
                                $jadwalV = $shift->nama_shift;
                                if ($shift->jam_pulang !== null) {
                                    if ($shift->jam_masuk !== null) {
                                        $jamMasuk = strtotime($shift->jam_masuk);
                                        $jamPulang = strtotime($shift->jam_pulang);
                                        $countHour += round(abs($jamPulang - $jamMasuk) / 3600, 2);
                                    }
                                }
                            }
                            if($jadwalV==='L'||$jadwalV==='L/M'){
                              echo '<td class="content-table center" id="liburs">' . $jadwalV . '</td>'; 
                            }else{
                              echo '<td class="content-table center">' . $jadwalV . '</td>';  
                            }
                            
                        }
                        ?>
                        <td class="content-table center"><?php echo $countHour; ?></td>
                        <td class="content-table"></td>
                    </tr>
                    <?php
                    $no++;
                endforeach;
            else :
                ?>
                <tr>
                    <td class="content-table center" colspan="<?php echo (7 + $jadwal['days']); ?>">Data tidak di temukan</td>
                </tr>
            <?php endif; ?>
            <tr>
            </tr>
        </table>
    </body>
</html>